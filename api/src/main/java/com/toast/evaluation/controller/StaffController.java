package com.toast.evaluation.controller;

import com.toast.evaluation.common.constant.Role;
import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.common.model.EvaResponse;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.vo.PasswordRequestVo;
import com.toast.evaluation.service.AuthenticationService;
import com.toast.evaluation.service.StaffService;
import com.toast.evaluation.service.UserService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/staff")
public class StaffController {

    private static final Logger logger = EvaLogManager.getLogger(StaffController.class);

    @Autowired
    private StaffService staffService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserService userService;

    @PreAuthorize("isAuthenticated() && hasRole('ADMIN')")
    @ResponseBody
    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<List<Staff>> getAllStaff() {
        logger.info("Get all staff list");
        return new EvaResponse<>(staffService.getAllStaff());
    }

    @PreAuthorize("isAuthenticated() && hasRole('ADMIN')")
    @ResponseBody
    @RequestMapping(value = "/role/{role}", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<List<Staff>> getStaffsByRole(@PathVariable("role") Role role) {
        logger.info("getStaffsByRole: {}", role);
        return new EvaResponse<>(staffService.getStaffsByRole(role));
    }

    @PreAuthorize("isAuthenticated() && hasRole('LEAD')")
    @ResponseBody
    @RequestMapping(value = "/member", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<List<Staff>> getStaffsMember() {
        Staff staff = authenticationService.getInfoStaffLogged();
        return new EvaResponse<>(staffService.getStaffsMember(staff.getStaffId()));
    }

    @PreAuthorize("isAuthenticated() && hasPermission('staffPermission', 'staff', #staffId)")
    @ResponseBody
    @RequestMapping(value = "/{staffId}", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<Staff> getStaffById(@PathVariable("staffId") int staffId) {
        logger.info("getStaffById: {}", staffId);
        return new EvaResponse<>(staffService.getStaffById(staffId));
    }

    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    @RequestMapping(value = "/detail", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<Staff> getStaffDetail() {
        logger.info("getStaffDetail");
        Staff staff = authenticationService.getInfoStaffLogged();
        return new EvaResponse<>(staffService.getStaffById(staff.getStaffId()));
    }

    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    @RequestMapping(value = "/lead", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<Staff> getLeaderByStaffId() {
        logger.info("getLeaderByStaffId");
        Staff staff = authenticationService.getInfoStaffLogged();
        return new EvaResponse<>(staffService.getLeaderByStaffId(staff.getStaffId()));
    }

    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    @RequestMapping(value = "/task", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<Map> getTask() {
        logger.info("getTask");
        Staff staff = authenticationService.getInfoStaffLogged();
        return new EvaResponse<>(staffService.getTask(staff));
    }

    @ResponseBody
    @RequestMapping(value = "/change-password", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    public EvaResponse changePassword(@RequestBody PasswordRequestVo passwordReqParam) {
        Staff staff = authenticationService.getInfoStaffLogged();
        logger.info("changePassword for {} ", staff.getStaffId());
        userService.changePassword(passwordReqParam);
        return new EvaResponse();
    }
}
