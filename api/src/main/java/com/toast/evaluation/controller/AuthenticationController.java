package com.toast.evaluation.controller;

import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.common.model.EvaResponse;
import com.toast.evaluation.model.vo.UserVo;
import com.toast.evaluation.service.UserService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {
    private static final Logger logger = EvaLogManager.getLogger(AuthenticationController.class);

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public EvaResponse login(@RequestBody UserVo user) {
        logger.info("login coStaffId: {} ", user.getCoStaffId());
        return userService.auth(user);
    }
}