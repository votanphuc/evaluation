package com.toast.evaluation.controller;

import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.common.model.EvaResponse;
import com.toast.evaluation.model.Period;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.evaluation.EvaluationHeader;
import com.toast.evaluation.model.vo.PeriodStaffVo;
import com.toast.evaluation.service.EvaluationService;
import com.toast.evaluation.service.PeriodService;
import com.toast.evaluation.service.StaffService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/period")
public class PeriodController {
    private static final Logger logger = EvaLogManager.getLogger(PeriodController.class);

    @Autowired
    private PeriodService periodService;

    @Autowired
    private StaffService staffService;

    @Autowired
    private EvaluationService evaluationService;

    @PreAuthorize("isAuthenticated() && hasRole('ADMIN')")
    @ResponseBody
    @RequestMapping(value = {"", "/"}, method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public EvaResponse createPeriod(@RequestBody PeriodStaffVo periodStaff) {
        logger.info("createPeriod");
        periodService.createPeriod(periodStaff);
        return new EvaResponse();
    }

    @PreAuthorize("isAuthenticated() && hasRole('ADMIN')")
    @ResponseBody
    @RequestMapping(value = "/{periodId}", method = RequestMethod.DELETE, produces = "application/json")
    public EvaResponse deletePeriod(@PathVariable("periodId") int periodId) {
        logger.info("deletePeriod periodId: {}", periodId);
        periodService.deletePeriod(periodId);
        return new EvaResponse();
    }

    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    @RequestMapping(value = "/{periodId}", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<Period> getPeriodById(@PathVariable("periodId") int periodId) {
        logger.info("getPeriodById periodId: {}", periodId);
        return new EvaResponse<>(periodService.getPeriodById(periodId));
    }

    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    @RequestMapping(value = "/current", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<Period> getCurrentPeriod() {
        logger.info("getCurrentPeriod");
        return new EvaResponse<>(periodService.getCurrentPeriod());
    }

    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<List<Period>> getAllPeriod() {
        logger.info("getAllPeriod");
        return new EvaResponse<>(periodService.getAllPeriod());
    }

    @PreAuthorize("isAuthenticated() && hasRole('ADMIN')")
    @ResponseBody
    @RequestMapping(value = "/{periodId}/staff", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<List<Staff>> getStaffsByPeriodId(@PathVariable("periodId") int periodId) {
        logger.info("getStaffsByPeriodId  periodId: {}", periodId);
        return new EvaResponse<>(staffService.getStaffsByPeriodId(periodId));
    }

    @PreAuthorize("isAuthenticated() && hasRole('ADMIN')")
    @ResponseBody
    @RequestMapping(value = "/{periodId}/staff/{staffId}", method = RequestMethod.DELETE, produces = "application/json")
    public EvaResponse deleteStaffFromPeriod(@PathVariable("periodId") int periodId, @PathVariable("staffId") int staffId) {
        logger.info("deleteStaffFromPeriod periodId: {} - staffId: {}", periodId, staffId);
        periodService.deleteStaffFromPeriod(periodId, staffId);
        return new EvaResponse();
    }

    @PreAuthorize("isAuthenticated() && hasPermission('staffPermission', 'staff', #staffId)")
    @ResponseBody
    @RequestMapping(value = "/{periodId}/staff/{staffId}/eva", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<List<EvaluationHeader>> getEvaluationsByStaffIdInPeriod(@PathVariable("staffId") int staffId,
                                                                               @PathVariable("periodId") int periodId) {
        logger.info("getEvaluationsByStaffIdInPeriod periodId: {} - staffId: {}", periodId, staffId);
        return new EvaResponse<>(evaluationService.getEvaluationsByStaffIdInPeriod(staffId, periodId));
    }

    @PreAuthorize("isAuthenticated() && hasPermission('staffPermission', 'staff', #staffId)")
    @ResponseBody
    @RequestMapping(value = "/current/staff/{staffId}/eva", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<List<EvaluationHeader>> getEvaluationsByStaffIdInCurrentPeriod(@PathVariable("staffId") int staffId) {
        logger.info("getEvaluationsByStaffIdInCurrentPeriod staffId {}", staffId);
        return new EvaResponse<>(evaluationService.getEvaluationsByStaffIdInCurrentPeriod(staffId));
    }

    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    @RequestMapping(value = "/eva-in-current", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<List<EvaluationHeader>> getAllEvaInCurrentPeriod() {
        logger.info("getAllEvaInCurrentPeriod");
        return new EvaResponse<>(evaluationService.getAllEvaInCurrentPeriod());
    }

    @PreAuthorize("isAuthenticated() && hasRole('ADMIN')")
    @ResponseBody
    @RequestMapping(value = "/{periodId}/eva", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<List<EvaluationHeader>> getAllEvaByPeriodId(@PathVariable("periodId") int periodId) {
        logger.info("getAllEvaByPeriodId: {}", periodId);
        return new EvaResponse<>(evaluationService.getAllEvaByPeriodId(periodId));
    }
}
