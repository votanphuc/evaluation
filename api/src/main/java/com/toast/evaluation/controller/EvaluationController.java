package com.toast.evaluation.controller;

import com.toast.evaluation.common.constant.EvaluationStatus;
import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.common.model.EvaResponse;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.evaluation.Evaluation;
import com.toast.evaluation.model.evaluation.EvaluationHeader;
import com.toast.evaluation.model.evaluation.Question;
import com.toast.evaluation.model.evaluation.ReopenEvaluation;
import com.toast.evaluation.service.AuthenticationService;
import com.toast.evaluation.service.EvaluationService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("eva")
public class EvaluationController {
    private static final Logger logger = EvaLogManager.getLogger(EvaluationController.class);

    @Autowired
    EvaluationService evaluationService;

    @Autowired
    AuthenticationService authenticationService;

    @PreAuthorize("isAuthenticated() && hasPermission('staffPermission', 'evaluation', #evaId)")
    @ResponseBody
    @RequestMapping(value = "/{evaId}/header", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<EvaluationHeader> getEvaluationHeaderById(@PathVariable("evaId") int evaId) {
        logger.info("getEvaluationHeaderById evaId: {}", evaId);
        return new EvaResponse<>(evaluationService.getEvaluationHeaderById(evaId));
    }

    @PreAuthorize("isAuthenticated() && hasPermission('staffPermission', 'evaluation', #evaId)")
    @ResponseBody
    @RequestMapping(value = "/{evaId}/content", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<List<Question>> getEvaluationContentById(@PathVariable("evaId") int evaId) {
        logger.info("getEvaluationContentById evaId: {}", evaId);
        return new EvaResponse<>(evaluationService.getEvaluationContentById(evaId));
    }

    @PreAuthorize("isAuthenticated() && hasPermission('staffPermission', 'evaluation', #evaId)")
    @ResponseBody
    @RequestMapping(value = "/{evaId}/reopen", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<ReopenEvaluation> getReopenInfoByEvaId(@PathVariable int evaId) {
        logger.info("getReopenInfoByEvaId evaId: {}", evaId);
        return new EvaResponse<>(evaluationService.getReopenInfoByEvaId(evaId));
    }

    @PreAuthorize("isAuthenticated() && hasPermission('staffPermission', 'staff', #staffId)")
    @ResponseBody
    @RequestMapping(value = "/staff/{staffId}", method = RequestMethod.GET, produces = "application/json")
    public EvaResponse<List<EvaluationHeader>> getEvaluationsByStaffId(@PathVariable("staffId") int staffId) {
        logger.info("getEvaluationsByStaffId staffId: {}", staffId);
        return new EvaResponse<>(evaluationService.getEvaluationsByStaffId(staffId));
    }

    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    @RequestMapping(value = "/submit", method = RequestMethod.POST, produces = "application/json")
    public EvaResponse submitEvaluation(@RequestBody Evaluation eva) {
        logger.info("submitEvaluation");
        Staff staff = authenticationService.getInfoStaffLogged();
        eva.getEvaHeader().setStaff(staff);

        evaluationService.submitEvaluation(eva);
        return new EvaResponse();
    }

    @PreAuthorize("isAuthenticated() && hasPermission('staffPermission', 'evaluation', #eva.evaHeader.evaluateId)")
    @ResponseBody
    @RequestMapping(value = "/submit", method = RequestMethod.PUT, produces = "application/json")
    public EvaResponse updateEvaluation(@RequestBody Evaluation eva) {
        logger.info("updateEvaluation");
        evaluationService.submitEvaluation(eva);
        return new EvaResponse();
    }

    @PreAuthorize("isAuthenticated() && hasRole('ADMIN')")
    @ResponseBody
    @RequestMapping(value = "/{evaId}", method = RequestMethod.POST, produces = "application/json")
    public EvaResponse changeStatus(@PathVariable int evaId, @RequestParam("status") EvaluationStatus evaStatus,
                                    @RequestParam(required = false, value = "startDtReopen") String startDtReopenStr,
                                    @RequestParam(required = false, value = "endDtReopen") String endDtReopenStr) {
        logger.info("changeStatus evaId: {}", evaId);
        evaluationService.changeStatus(evaId, evaStatus, startDtReopenStr, endDtReopenStr);
        return new EvaResponse();
    }

    @PreAuthorize("isAuthenticated() && hasRole('ADMIN')")
    @ResponseBody
    @RequestMapping(value = "/{evaId}", method = RequestMethod.DELETE, produces = "application/json")
    public EvaResponse deleteEvaluation(@PathVariable int evaId) {
        logger.info("deleteEvaluation evaId: {}", evaId);
        evaluationService.deleteEvaluation(evaId);
        return new EvaResponse();
    }

    @PreAuthorize("isAuthenticated() && hasRole('CEO')")
    @ResponseBody
    @RequestMapping(value = "/submit-result", method = RequestMethod.PUT, produces = "application/json")
    public EvaResponse submitResult(@RequestBody EvaluationHeader evaluationHeader) {
        logger.info("submitResult");
        evaluationService.submitResult(evaluationHeader);
        return new EvaResponse();
    }
}
