package com.toast.evaluation.dao.impl;

import com.toast.evaluation.common.constant.Role;
import com.toast.evaluation.dao.StaffDao;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.Team;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StaffDaoImpl extends BaseDaoImpl implements StaffDao {

    @Override
    public void setPackageName() {
        packageName = "staff.";
    }

    @Override
    public List<Staff> getAllStaff() {
        return sqlSession.selectList(getPackageName() + "getAllStaff");
    }

    @Override
    public Staff getStaffById(int staffId) {
        return sqlSession.selectOne(getPackageName() + "getStaffById", staffId);
    }

    @Override
    public List<Staff> getStaffsByPeriodId(int periodId) {
        return sqlSession.selectList(getPackageName() + "getStaffsByPeriodId", periodId);
    }

    @Override
    public Staff getLeaderByStaffId(int staffId) {
        return sqlSession.selectOne(getPackageName() + "getLeaderByStaffId", staffId);
    }

    @Override
    public List<Staff> getStaffsByRole(Role role) {
        return sqlSession.selectList(getPackageName() + "getStaffsByRole", role);
    }

    @Override
    public List<Staff> getStaffsMember(int staffId) {
        return sqlSession.selectList(getPackageName() + "getStaffsMember", staffId);
    }

    @Override
    public Team getTeamFromStaffId(int staffId) {
        return sqlSession.selectOne(getPackageName() + "getTeamFromStaffId", staffId);
    }
}