package com.toast.evaluation.dao;

import com.toast.evaluation.common.constant.EvaluationStatus;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.evaluation.EvaluationHeader;
import com.toast.evaluation.model.evaluation.Question;
import com.toast.evaluation.model.evaluation.ReopenEvaluation;

import java.util.List;

public interface EvaluationDao {

    List<EvaluationHeader> getEvaluationsByStaffId(int staffId);

    List<EvaluationHeader> getEvaluationsByStaffIdInPeriod(int staffId, int periodId);

    EvaluationHeader getEvaluationHeaderById(int evaId);

    List<Question> getEvaluationContentById(int evaId);

    void submitEvaluation(EvaluationHeader evaHeader, List<Question> questions);

    void changeStatus(int evaId, EvaluationStatus evaStatus);

    void reopenEvaluate(ReopenEvaluation reopenEva);

    Staff getStaffByEvaId(int evaId);

    void deleteEvaluation(int evaId);

    ReopenEvaluation getReopenInfoByEvaId(int evaId);

    List<EvaluationHeader> getEvaluationsOfStaffByAdmin();

    List<EvaluationHeader> getEvaluationsOfMemberByTeamId(int teamId);

    void submitResult(EvaluationHeader evaluationHeader);

    List<EvaluationHeader> getAllEvaByPeriodId(int periodId);
}

