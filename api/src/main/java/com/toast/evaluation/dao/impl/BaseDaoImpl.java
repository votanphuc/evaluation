package com.toast.evaluation.dao.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseDaoImpl {
    @Autowired
    protected SqlSessionTemplate sqlSession;

    protected String packageName;

    public BaseDaoImpl() {
        setPackageName();
    }

    public SqlSessionTemplate getSqlSession() {
        return sqlSession;
    }

    public void setSqlSession(SqlSessionTemplate sqlSession) {
        this.sqlSession = sqlSession;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public abstract void setPackageName();


}
