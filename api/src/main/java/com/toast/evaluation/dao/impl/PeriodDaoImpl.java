package com.toast.evaluation.dao.impl;

import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.dao.PeriodDao;
import com.toast.evaluation.model.Period;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PeriodDaoImpl extends BaseDaoImpl implements PeriodDao {
    private static final Logger logger = EvaLogManager.getLogger(PeriodDaoImpl.class);

    @Override
    public void setPackageName() {
        packageName = "period.";
    }

    @Override
    public List<Period> getAllPeriod() {
        return sqlSession.selectList(getPackageName() + "getAllPeriod");
    }

    @Override
    public Period getPeriodById(int periodId) {
        return sqlSession.selectOne(getPackageName() + "getPeriodById", periodId);
    }

    @Transactional
    @Override
    public void createPeriod(Period period, List staffsId) {
        int periodId = period.getPeriodId();
        sqlSession.insert(getPackageName() + "createPeriod", period);
        if (periodId == 0) {
            logger.info("Create Period: ", periodId);
            periodId = period.getPeriodId();
        } else {
            logger.info("Edit Period: ", periodId);
            deleteStaffsFromPeriod(periodId);
        }
        addStaffsInPeriod(periodId, staffsId);
    }

    @Transactional
    @Override
    public void deletePeriod(int periodId) {
        deleteStaffsFromPeriod(periodId);
        sqlSession.update(getPackageName() + "deletePeriod", periodId);
    }

    @Transactional
    @Override
    public void deleteStaffFromPeriod(int periodId, int staffId) {
        Map<String, Object> params = new HashMap<>();
        params.put("periodId", periodId);
        params.put("staffId", staffId);
        sqlSession.delete(getPackageName() + "deleteStaffFromPeriod", params);
    }

    @Override
    public Period getLastPeriod() {
        return sqlSession.selectOne(getPackageName() + "getLastPeriod");
    }

    @Override
    public Period getCurrentPeriod() {
        return sqlSession.selectOne(getPackageName() + "getCurrentPeriod");
    }

    private void addStaffsInPeriod(int periodId, List staffsId) {
        if (staffsId == null) {
            logger.warn("List staffId is null!");
            return;
        }
        Map<String, Object> params = new HashMap<>();
        params.put("periodId", periodId);
        params.put("staffsId", staffsId);

        sqlSession.insert(getPackageName() + "addStaffsInPeriod", params);
    }

    private void deleteStaffsFromPeriod(int periodId) {
        sqlSession.delete(getPackageName() + "deleteStaffsFromPeriod", periodId);
    }
}
