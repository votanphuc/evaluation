package com.toast.evaluation.dao.impl;

import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.dao.UserDao;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.vo.PasswordRequestVo;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Repository
public class UserDaoImpl extends BaseDaoImpl implements UserDao {

    private static final Logger logger = EvaLogManager.getLogger(EvaluationDaoImpl.class);

    @Override
    public void setPackageName() {
        packageName = "user.";
    }

    @Override
    public Staff findByCoStaffIdAndPassword(String coStaffId, String password) {
        Map<String, String> params = new HashMap<>();
        params.put("coStaffId", coStaffId);
        params.put("password", password);
        return sqlSession.selectOne(getPackageName() + "findByCoStaffIdAndPassword", params);
    }

    @Transactional
    @Override
    public void changePassword(PasswordRequestVo passwordReqParam) {
        sqlSession.update(getPackageName()+"changePassword", passwordReqParam);
    }

    @Override
    public int countStaffByIdAndPassword(int staffId, String password) {
        Map<String, Object> params = new HashMap<>();
        params.put("staffId", staffId);
        params.put("password", password);
        return sqlSession.selectOne(getPackageName() + "countUserByIdAndPass", params);
    }
}
