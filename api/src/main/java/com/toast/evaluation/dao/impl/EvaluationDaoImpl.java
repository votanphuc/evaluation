package com.toast.evaluation.dao.impl;

import com.toast.evaluation.common.constant.EvaluationStatus;
import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.dao.EvaluationDao;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.evaluation.EvaluationHeader;
import com.toast.evaluation.model.evaluation.Question;
import com.toast.evaluation.model.evaluation.ReopenEvaluation;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EvaluationDaoImpl extends BaseDaoImpl implements EvaluationDao {

    private static final Logger logger = EvaLogManager.getLogger(EvaluationDaoImpl.class);

    @Override
    public void setPackageName() {
        packageName = "eva.";
    }

    @Override
    public List<EvaluationHeader> getEvaluationsByStaffId(int staffId) {
        return sqlSession.selectList(getPackageName() + "getEvaluationsByStaffId", staffId);
    }

    @Override
    public List<EvaluationHeader> getEvaluationsByStaffIdInPeriod(int staffId, int periodId) {
        Map<String, Object> params = new HashMap<>();
        params.put("staffId", staffId);
        params.put("periodId", periodId);
        return sqlSession.selectList(getPackageName() + "getEvaluationsByStaffIdInPeriod", params);
    }

    @Override
    public EvaluationHeader getEvaluationHeaderById(int evaId) {
        return sqlSession.selectOne(getPackageName() + "getEvaluationHeaderById", evaId);
    }

    @Override
    public List<Question> getEvaluationContentById(int evaId) {
        return sqlSession.selectList(getPackageName() + "getEvaluationContentById", evaId);
    }

    @Transactional
    @Override
    public void submitEvaluation(EvaluationHeader evaHeader, List<Question> questions) {
        int evaId = evaHeader.getEvaluateId();
        sqlSession.insert(getPackageName() + "submitEvaluationHeader", evaHeader);
        if (evaId == 0) {
            evaId = evaHeader.getEvaluateId();
        }
        submitEvaluationContent(evaId, questions);
    }

    private void submitEvaluationContent(int evaId, List<Question> questions) {
        logger.info("Modify Evaluate content");
        Map<String, Object> params = new HashMap<>();

        for (Question question : questions) {
            params.clear();
            params.put("evaId", evaId);
            params.put("question", question);

            sqlSession.insert(getPackageName() + "submitEvaluationContent", params);
        }
    }

    @Transactional
    @Override
    public void changeStatus(int evaId, EvaluationStatus evaStatus) {
        Map<String, Object> params = new HashMap<>();
        params.put("evaId", evaId);
        params.put("evaStatus", evaStatus);
        sqlSession.update(getPackageName() + "changeStatus", params);
    }

    @Transactional
    @Override
    public void reopenEvaluate(ReopenEvaluation reopenEva) {
        sqlSession.insert(getPackageName() + "reopenEvaluation", reopenEva);
    }

    @Override
    public Staff getStaffByEvaId(int evaId) {
        return sqlSession.selectOne(getPackageName() + "getStaffByEvaId", evaId);
    }

    @Override
    public void deleteEvaluation(int evaId) {
        sqlSession.update(getPackageName() + "deleteEvaluation", evaId);
    }

    @Override
    public ReopenEvaluation getReopenInfoByEvaId(int evaId) {
        return sqlSession.selectOne(getPackageName() + "getReopenInfoByEvaId", evaId);
    }

    @Override
    public List<EvaluationHeader> getEvaluationsOfStaffByAdmin() {
        return sqlSession.selectList(getPackageName() + "getEvaluationsOfStaffByAdmin");
    }

    @Override
    public List<EvaluationHeader> getEvaluationsOfMemberByTeamId(int teamId) {
        return sqlSession.selectList(getPackageName() + "getEvaluationsOfMemberByTeamId", teamId);
    }

    @Transactional
    @Override
    public void submitResult(EvaluationHeader evaluationHeader) {
        sqlSession.update(getPackageName() + "submitResult" + evaluationHeader);
    }

    @Override
    public List<EvaluationHeader> getAllEvaByPeriodId(int periodId) {
        return sqlSession.selectList(getPackageName() + "getAllEvaByPeriodId", periodId);
    }

//    private void submitEvaluationAnswer(List<Question> questions) {
//        logger.info("Modify answer");
//        for (Question question : questions) {
//            sqlSession.insert(getPackageName() + "submitEvaluationAnswer", question.getAnswer());
//        }
//    }
//
//    private void deleteEvaluationContent(int evaId) {
//        logger.info("Delete EvaQuesAnswer");
//        sqlSession.delete(getPackageName() + "deleteEvaluationContent", evaId);
//    }
//
//    private void addEvaluationContent(int evaId, List<Question> questions) {
//        logger.info("Add EvaQuesAnswer");
//        Map<String, Object> params = new HashMap<>();
//        params.put("evaId", evaId);
//        params.put("questions", questions);
//
//        sqlSession.insert(getPackageName() + "addEvaluationContent", params);
//    }

}
