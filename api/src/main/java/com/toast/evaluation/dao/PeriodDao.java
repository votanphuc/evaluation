package com.toast.evaluation.dao;

import com.toast.evaluation.model.Period;

import java.util.List;

public interface PeriodDao {

    List<Period> getAllPeriod();

    Period getPeriodById(int periodId);

    void createPeriod(Period period, List staffsId);

    void deletePeriod(int periodId);

    void deleteStaffFromPeriod(int periodId, int staffId);

    Period getLastPeriod();

    Period getCurrentPeriod();
}
