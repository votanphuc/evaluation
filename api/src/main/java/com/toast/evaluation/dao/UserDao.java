package com.toast.evaluation.dao;

import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.vo.PasswordRequestVo;

public interface UserDao {

    Staff findByCoStaffIdAndPassword(String coStaffId, String password);

    void changePassword(PasswordRequestVo passwordReqParam);

    int countStaffByIdAndPassword(int staffId, String password);
}
