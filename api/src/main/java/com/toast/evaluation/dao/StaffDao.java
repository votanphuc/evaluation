package com.toast.evaluation.dao;

import com.toast.evaluation.common.constant.Role;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.Team;

import java.util.List;

public interface StaffDao {

    List<Staff> getAllStaff();

    Staff getStaffById(int staffId);

    List<Staff> getStaffsByPeriodId(int periodId);

    Staff getLeaderByStaffId(int staffId);

    List<Staff> getStaffsByRole(Role role);

    List<Staff> getStaffsMember(int staffId);

    Team getTeamFromStaffId(int staffId);
}
