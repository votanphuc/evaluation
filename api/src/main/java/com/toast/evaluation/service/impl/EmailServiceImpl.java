package com.toast.evaluation.service.impl;

import com.toast.evaluation.common.model.EvaEmail;
import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.common.utils.EvaMailUtils;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.service.EmailService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Properties;

@Component
public class EmailServiceImpl implements EmailService {
    private static final Logger logger = EvaLogManager.getLogger(EmailServiceImpl.class);

    private static final String EMAIL_HTML_TYPE = "text/html; charset=utf8";

    private Properties properties;
    @Value("${email.host}")
    private String host;

    @Value("${email.port}")
    private String port;

    @Value("${email.from}")
    private String sender;

    @Value("${email.password}")
    private String password;

    @Value("${spring.profiles.active}")
    private String env;


    @PostConstruct
    public void init() {
        properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
    }

    @Override
    public void sendNotiViaEmail(Staff staff, String startDtReopenStr, String endDtReopenStr) throws Exception {
        String title = "Noti for user " + staff.getStaffName();
        String message = "This message is body of emaillllllllllllllllllll";

        EvaMailUtils.send(properties, getEvaEmail(staff.getEmail(), title, message));
        logger.info("Sent registration email to [{}] success", staff.getEmail());
    }

    private EvaEmail getEvaEmail(String receiver, String title, String body) {
        return new EvaEmail() {
            @Override
            public String getUsername() {
                return sender;
            }

            @Override
            public String getPassword() {
                return password;
            }

            @Override
            public String getReceiver() {
                return receiver;
            }

            @Override
            public String getSubject() {
                return title;
            }

            @Override
            public String getContent() {
                return body;
            }

            @Override
            public String getContentType() {
                return EMAIL_HTML_TYPE;
            }
        };
    }
}
