package com.toast.evaluation.service;

import com.toast.evaluation.common.constant.ReturnCode;
import com.toast.evaluation.common.constant.Role;
import com.toast.evaluation.common.exception.RegisterException;
import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.evaluation.EvaluationHeader;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class StaffPermission implements PermissionEvaluator {
    private static final Logger logger = EvaLogManager.getLogger(StaffPermission.class);

    @Autowired
    StaffService staffService;

    @Autowired
    EvaluationService evaluationService;

    @Override
    public boolean hasPermission(Authentication authentication, Object o, Object o1) {
        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable serializable, String s, Object o) {
        logger.info("Check permission for id: {}, type of permission: {}", o, s);
        if ("staff".equals(s)) {
            Staff staff = (Staff) authentication.getPrincipal();
            return checkPermissionByStaffId(staff, (int) o);
        }

        if ("evaluation".equals(s)) {
            EvaluationHeader evaluationHeader = evaluationService.getEvaluationHeaderById((int) o);
            if (evaluationHeader == null) {
                throw new RegisterException(ReturnCode.DO_NOT_HAVE_THIS_EVALUATION_ID);
            }

            int staffId = evaluationHeader.getStaff().getStaffId();
            Staff staff = (Staff) authentication.getPrincipal();
            return checkPermissionByStaffId(staff, staffId);
        }
        return false;
    }

    private boolean checkPermissionByStaffId(Staff staff, int staffId) {
        Role role = staff.getRole();

        if (Role.ADMIN.equals(role)) {
            return true;
        }

        if (Role.LEAD.equals(role)) {
            Staff lead = staffService.getLeaderByStaffId(staffId);
            if (lead == null) {
                throw new RegisterException(ReturnCode.NO_LEADER);
            }
            return staff.getStaffId() == lead.getStaffId();
        }

        return staffId == staff.getStaffId();
    }

}
