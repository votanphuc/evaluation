package com.toast.evaluation.service.impl;

import com.toast.evaluation.dao.PeriodDao;
import com.toast.evaluation.model.Period;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.vo.PeriodStaffVo;
import com.toast.evaluation.service.AuthenticationService;
import com.toast.evaluation.service.PeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeriodServiceImpl implements PeriodService {
    @Autowired
    PeriodDao periodDao;

    @Autowired
    AuthenticationService authenticationService;

    @Override
    public List<Period> getAllPeriod() {
        return periodDao.getAllPeriod();
    }

    @Override
    public Period getPeriodById(int periodId) {
        return periodDao.getPeriodById(periodId);
    }

    @Override
    public void createPeriod(PeriodStaffVo periodStaff) {
        Staff staff = authenticationService.getInfoStaffLogged();
        periodStaff.getPeriod().setCreator(staff);
        periodDao.createPeriod(periodStaff.getPeriod(), periodStaff.getStaffsId());
    }

    @Override
    public void deletePeriod(int periodId) {
        periodDao.deletePeriod(periodId);
    }

    @Override
    public void deleteStaffFromPeriod(int periodId, int staffId) {
        periodDao.deleteStaffFromPeriod(periodId, staffId);
    }

    @Override
    public Period getCurrentPeriod() {
        return periodDao.getCurrentPeriod();
    }
}
