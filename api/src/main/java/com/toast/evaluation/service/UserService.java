package com.toast.evaluation.service;

import com.toast.evaluation.common.model.EvaResponse;
import com.toast.evaluation.model.vo.PasswordRequestVo;
import com.toast.evaluation.model.vo.UserVo;

public interface UserService {

    EvaResponse auth(UserVo user);

    void changePassword(PasswordRequestVo passwordReqParam);
}
