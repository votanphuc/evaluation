package com.toast.evaluation.service.impl;

import com.toast.evaluation.common.constant.Role;
import com.toast.evaluation.dao.PeriodDao;
import com.toast.evaluation.dao.StaffDao;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.Team;
import com.toast.evaluation.service.EvaluationService;
import com.toast.evaluation.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StaffServiceImpl implements StaffService {

    @Autowired
    StaffDao staffDao;

    @Autowired
    PeriodDao periodDao;

    @Autowired
    EvaluationService evaluationService;

    @Override
    public List<Staff> getAllStaff() {
        return staffDao.getAllStaff();
    }

    @Override
    public Staff getStaffById(int staffId) {
        return staffDao.getStaffById(staffId);
    }

    @Override
    public List<Staff> getStaffsByPeriodId(int periodId) {
        return staffDao.getStaffsByPeriodId(periodId);
    }

    @Override
    public Staff getLeaderByStaffId(int staffId) {
        return staffDao.getLeaderByStaffId(staffId);
    }

    @Override
    public List<Staff> getStaffsByRole(Role role) {
        return staffDao.getStaffsByRole(role);
    }

    @Override
    public List<Staff> getStaffsMember(int staffId) {
        return staffDao.getStaffsMember(staffId);
    }

    @Override
    public Map getTask(Staff staff) {
        switch (staff.getRole()) {
            case ADMIN:
                return getTaskAdmin();
            case LEAD:
                return getTaskLead(staff.getStaffId());
            case STAFF:
                return getTaskStaff(staff.getStaffId());
        }
        return null;
    }

    @Override
    public Team getTeamFromStaffId(int leaderId) {
        return staffDao.getTeamFromStaffId(leaderId);
    }

    private Map getTaskAdmin() {
        Map<String, Object> params = new HashMap<>();
        params.put("task1", getAllStaff());
        return params;
    }

    private Map getTaskLead(int staffId) {
        Map<String, Object> params = new HashMap<>();
        params.put("task1", getStaffsMember(staffId));
        params.put("task2", evaluationService.getEvaluationsByStaffIdInCurrentPeriod(staffId));
        return params;
    }

    private Map getTaskStaff(int staffId) {
        Map<String, Object> params = new HashMap<>();
        params.put("task1", evaluationService.getEvaluationsByStaffIdInCurrentPeriod(staffId));
        return params;
    }


}
