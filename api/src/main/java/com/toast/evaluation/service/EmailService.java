package com.toast.evaluation.service;

import com.toast.evaluation.model.Staff;

public interface EmailService {

    void sendNotiViaEmail(Staff staff, String startDtReopenStr, String endDtReopenStr) throws Exception;
}
