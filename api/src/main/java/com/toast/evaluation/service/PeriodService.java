package com.toast.evaluation.service;

import com.toast.evaluation.model.Period;
import com.toast.evaluation.model.vo.PeriodStaffVo;

import java.util.List;

public interface PeriodService {

    List<Period> getAllPeriod();

    void createPeriod(PeriodStaffVo periodStaff);

    Period getPeriodById(int periodId);

    void deletePeriod(int periodId);

    void deleteStaffFromPeriod(int periodId, int staffId);

    Period getCurrentPeriod();
}
