package com.toast.evaluation.service.impl;

import com.toast.evaluation.common.constant.EvaluationStatus;
import com.toast.evaluation.common.constant.ReturnCode;
import com.toast.evaluation.common.constant.Role;
import com.toast.evaluation.common.exception.EvaException;
import com.toast.evaluation.common.exception.RegisterException;
import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.dao.EvaluationDao;
import com.toast.evaluation.model.Period;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.Team;
import com.toast.evaluation.model.evaluation.Evaluation;
import com.toast.evaluation.model.evaluation.EvaluationHeader;
import com.toast.evaluation.model.evaluation.Question;
import com.toast.evaluation.model.evaluation.ReopenEvaluation;
import com.toast.evaluation.service.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class EvaluationServiceImpl implements EvaluationService {
    public static final Logger logger = EvaLogManager.getLogger(EvaluationServiceImpl.class);

    @Autowired
    EvaluationDao evaluationDao;

    @Autowired
    PeriodService periodService;

    @Autowired
    StaffService staffService;

    @Autowired
    EmailService emailService;

    @Autowired
    AuthenticationService authenticationService;

    @Override
    public List<EvaluationHeader> getEvaluationsByStaffId(int staffId) {
        return evaluationDao.getEvaluationsByStaffId(staffId);
    }

    @Override
    public List<EvaluationHeader> getEvaluationsByStaffIdInPeriod(int staffId, int periodId) {
        return evaluationDao.getEvaluationsByStaffIdInPeriod(staffId, periodId);

    }

    @Override
    public EvaluationHeader getEvaluationHeaderById(int evaId) {
        return evaluationDao.getEvaluationHeaderById(evaId);
    }

    @Override
    public List<Question> getEvaluationContentById(int evaId) {
        Staff staff = authenticationService.getInfoStaffLogged();
        List<Question> content = evaluationDao.getEvaluationContentById(evaId);
        if (Role.STAFF.equals(staff.getRole())) {
            for (Question question : content) {
                question.getAnswer().setReview("");
            }
        }
        return content;
    }

    @Override
    public void submitEvaluation(Evaluation eva) {
        Staff staff = authenticationService.getInfoStaffLogged();
        if (Role.STAFF.equals(staff.getRole())) {
            for (Question question : eva.getQuestions()) {
                question.getAnswer().setReview(null);
                question.getAnswer().setFeedback(null);
            }
        }
        Period period = periodService.getCurrentPeriod();
        eva.getEvaHeader().setPeriod(period);
        evaluationDao.submitEvaluation(eva.getEvaHeader(), eva.getQuestions());
    }

    @Transactional
    @Override
    public void changeStatus(int evaId, EvaluationStatus evaStatus, String startDtReopenStr, String endDtReopenStr) {
        evaluationDao.changeStatus(evaId, evaStatus);

        if (evaStatus.equals(EvaluationStatus.REOPEN)) {
            logger.info("Reopen - s:{} - e:{}", startDtReopenStr, endDtReopenStr);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date startDtReopen = df.parse(startDtReopenStr);
                Date endDtReopen = df.parse(endDtReopenStr);
                ReopenEvaluation reopenEva = new ReopenEvaluation(evaId, startDtReopen, endDtReopen);
                evaluationDao.reopenEvaluate(reopenEva);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sendNotiViaEmail(evaId, startDtReopenStr, endDtReopenStr);
        }
    }

    @Override
    public void deleteEvaluation(int evaId) {
        evaluationDao.deleteEvaluation(evaId);
    }

    @Override
    public ReopenEvaluation getReopenInfoByEvaId(int evaId) {
        EvaluationHeader evaluationHeader = evaluationDao.getEvaluationHeaderById(evaId);
        if (evaluationHeader.getStatus() != EvaluationStatus.REOPEN) {
            logger.info("Status of evaluateId {} is not REOPEN", evaId);
            throw new RegisterException(ReturnCode.EVALUATION_IS_NOT_REOPEN);
        }
        return evaluationDao.getReopenInfoByEvaId(evaId);
    }

    @Override
    public List<EvaluationHeader> getEvaluationsByStaffIdInCurrentPeriod(int staffId) {
        Period period = periodService.getCurrentPeriod();
        if (period == null) {
            throw new RegisterException(ReturnCode.CURRENT_HAVE_NO_PERIOD);
        }
        int periodId = period.getPeriodId();
        List<Staff> staffs = staffService.getStaffsByPeriodId(periodId);
        for (Staff s : staffs) {
            if (staffId == s.getStaffId()) {
                return evaluationDao.getEvaluationsByStaffIdInPeriod(staffId, periodId);
            }
        }
        throw new RegisterException(ReturnCode.STAFF_IS_NOT_IN_PERIOD);
    }

    @Override
    public List<EvaluationHeader> getAllEvaInCurrentPeriod() {
        Staff staff = authenticationService.getInfoStaffLogged();
        switch (staff.getRole()) { //TODO: check business here!
            case CEO:
            case ADMIN:
                return getEvaluationsOfStaffByAdmin();
            case LEAD:
                return getEvaluationsOfMemberByLeaderId(staff.getStaffId());
        }
        return null;
    }

    @Transactional
    @Override
    public void submitResult(EvaluationHeader evaluationHeader) {
        evaluationDao.submitResult(evaluationHeader);
    }

    @Override
    public List<EvaluationHeader> getAllEvaByPeriodId(int periodId) {
        return evaluationDao.getAllEvaByPeriodId(periodId);
    }

    private List<EvaluationHeader> getEvaluationsOfStaffByAdmin() {
        return evaluationDao.getEvaluationsOfStaffByAdmin();
    }

    private List<EvaluationHeader> getEvaluationsOfMemberByLeaderId(int leaderId) {
        Team team = staffService.getTeamFromStaffId(leaderId);
        return evaluationDao.getEvaluationsOfMemberByTeamId(team.getTeamId());
    }

    private void sendNotiViaEmail(int evaId, String startDtReopenStr, String endDtReopenStr) {
        Staff staff = evaluationDao.getStaffByEvaId(evaId);
        try {
            emailService.sendNotiViaEmail(staff, startDtReopenStr, endDtReopenStr);
        } catch (Exception e) {
            logger.error("Cannot send email");
            throw new EvaException().setCode(ReturnCode.INTERNAL_SERVER_ERROR);
        }
    }


}
