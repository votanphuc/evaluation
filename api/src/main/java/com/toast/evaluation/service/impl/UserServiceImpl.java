package com.toast.evaluation.service.impl;

import com.toast.evaluation.common.constant.ReturnCode;
import com.toast.evaluation.common.exception.EvaException;
import com.toast.evaluation.common.exception.RegisterException;
import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.common.model.EvaResponse;
import com.toast.evaluation.common.utils.JwtUtils;
import com.toast.evaluation.dao.UserDao;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.vo.PasswordRequestVo;
import com.toast.evaluation.model.vo.UserVo;
import com.toast.evaluation.service.UserService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = EvaLogManager.getLogger(UserServiceImpl.class);

    @Autowired
    UserDao userDao;

    private JwtUtils jwtUtils = new JwtUtils();

    @Override
    public EvaResponse auth(UserVo user) {
        //find coStaffId/password match
        Staff staff = userDao.findByCoStaffIdAndPassword(user.getCoStaffId(), user.getPassword());

        if(staff == null){
            logger.error("User not found");
            throw new RegisterException(ReturnCode.COSTAFFID_PW_NOT_FOUND);
        }
        return generateJwt(staff);
    }

    @Override
    public void changePassword(PasswordRequestVo passwordReqParam) {
        int staffId = passwordReqParam.getStaffId();
        String currentPassword = passwordReqParam.getCurrentPassword();
        boolean valid = validateUserAndPassword(staffId, currentPassword);
        if(!valid){
            throw new EvaException().setCode(ReturnCode.INCORRECT_PASSWORD);
        }
        userDao.changePassword(passwordReqParam);
    }

    private EvaResponse generateJwt(Staff staff) {
        String jwt = jwtUtils.encode(staff);
        String encoded = Base64.getEncoder().encodeToString(jwt.getBytes());
        logger.info("Token: {}", encoded);
        return new EvaResponse(encoded) ;
    }


    private boolean validateUserAndPassword(int staffId, String password) {
        int count = userDao.countStaffByIdAndPassword(staffId, password);
        return count == 1;
    }
}
