package com.toast.evaluation.service;

import com.toast.evaluation.model.Staff;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {
    public Staff getInfoStaffLogged() {
        Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (object instanceof Staff) {
            return (Staff) object;
        }
        return null;
    }
}
