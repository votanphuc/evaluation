package com.toast.evaluation.service;

import com.toast.evaluation.common.constant.EvaluationStatus;
import com.toast.evaluation.model.evaluation.EvaluationHeader;
import com.toast.evaluation.model.evaluation.Question;
import com.toast.evaluation.model.evaluation.ReopenEvaluation;
import com.toast.evaluation.model.evaluation.Evaluation;

import java.util.List;

public interface EvaluationService {

    List<EvaluationHeader> getEvaluationsByStaffId(int staffId);

    List<EvaluationHeader> getEvaluationsByStaffIdInPeriod(int staffId, int periodId);

    EvaluationHeader getEvaluationHeaderById(int evaId);

    List<Question> getEvaluationContentById(int evaId);

    void submitEvaluation(Evaluation eva);

    void changeStatus(int evaId, EvaluationStatus evaStatus, String startDtReopenStr, String endDtReopenStr);

    void deleteEvaluation(int evaId);

    ReopenEvaluation getReopenInfoByEvaId(int evaId);

    List<EvaluationHeader> getEvaluationsByStaffIdInCurrentPeriod(int staffId);

    List<EvaluationHeader> getAllEvaInCurrentPeriod();

    void submitResult(EvaluationHeader evaluationHeader);

    List<EvaluationHeader> getAllEvaByPeriodId(int periodId);
}
