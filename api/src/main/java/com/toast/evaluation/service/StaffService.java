package com.toast.evaluation.service;

import com.toast.evaluation.common.constant.Role;
import com.toast.evaluation.model.Staff;
import com.toast.evaluation.model.Team;

import java.util.List;
import java.util.Map;

public interface StaffService {

    List<Staff> getAllStaff();

    Staff getStaffById(int staffId);

    List<Staff> getStaffsByPeriodId(int periodId);

    Staff getLeaderByStaffId(int staffId);

    List<Staff> getStaffsByRole(Role role);

    List<Staff> getStaffsMember(int staffId);

    Map getTask(Staff staff);

    Team getTeamFromStaffId(int leaderId);
}