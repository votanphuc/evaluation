package com.toast.evaluation.security;

import com.toast.evaluation.common.constant.ReturnCode;
import com.toast.evaluation.common.exception.EvaException;
import com.toast.evaluation.common.model.EvaHeader;
import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.common.model.EvaResponse;
import com.toast.evaluation.common.utils.JwtUtils;
import com.toast.evaluation.model.Staff;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Component
public class CustomTokenFilter extends GenericFilterBean {
    private static final Logger logger = EvaLogManager.getLogger(CustomTokenFilter.class);
    private static final String TOKEN_KEY = "token";

    private JwtUtils jwtUtils = new JwtUtils();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (!(servletRequest instanceof HttpServletRequest)) {
            throw new EvaException(ReturnCode.BAD_REQUEST, "Expecting a HTTP request");
        }

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String token = httpServletRequest.getParameter(TOKEN_KEY);

        if (token == null || token.isEmpty()) {
            token = httpServletRequest.getHeader(TOKEN_KEY);
        }

        if (token != null && !token.isEmpty()) {
            Staff staff;
            try {
                byte[] bytes = Base64.getDecoder().decode(token.trim());
                String decode = new String(bytes);
                staff = jwtUtils.decode(decode);
            } catch (Exception e) {
                logger.warn("Invalid token: {}", e.getMessage());

                EvaResponse<Object> error = buildInvalidTokenError();
                HttpServletResponse res = (HttpServletResponse) servletResponse;
                res.setStatus(401);
                res.setContentType("application/json");

                PrintWriter writer = res.getWriter();
                writer.print(error.toJson());
                writer.flush();

                return;
            }
            if (staff != null) {
                List<GrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new SimpleGrantedAuthority("ROLE_" + staff.getRole().name()));

                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(staff, null, authorities);
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) servletRequest));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private EvaResponse<Object> buildInvalidTokenError() {
        EvaHeader header = new EvaHeader(ReturnCode.INVALID_TOKEN);
        EvaResponse<Object> evaResponse = new EvaResponse<>();
        evaResponse.setHeader(header);
        return evaResponse;
    }
}
