package com.toast.evaluation.common.utils;

import com.toast.evaluation.common.model.EvaEmail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EvaMailUtils {
    public EvaMailUtils() {
    }
    public static final void send(Properties properties, EvaEmail email) throws MessagingException {
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email.getUsername(), email.getPassword());
            }
        };

        Session session = Session.getInstance(properties, authenticator);

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(email.getUsername()));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email.getReceiver()));
        message.setSubject(email.getSubject());
        message.setContent(email.getContent(), email.getContentType());
        Transport.send(message);
    }
}
