package com.toast.evaluation.common.exception;

import com.toast.evaluation.common.constant.ReturnCode;

public class EvaException extends RuntimeException {
    private ReturnCode code;

    public EvaException() {
    }

    public EvaException(String msg) {
        super(msg);
    }

    public EvaException(ReturnCode code, String msg) {
        super(msg);
        this.code = code;
    }

    public ReturnCode getCode() {
        return code;
    }

    public EvaException setCode(ReturnCode code) {
        this.code = code;
        return this;
    }
}
