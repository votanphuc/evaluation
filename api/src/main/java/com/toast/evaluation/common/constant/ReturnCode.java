package com.toast.evaluation.common.constant;

public enum ReturnCode {
    SUCCESS(200),
    BAD_REQUEST(400),
    UNAUTHORIZE(401),
    FORBIDDEN(403),
    INTERNAL_SERVER_ERROR(500),

    INVALID_TOKEN(-10001),

    USER_IS_NOT_VALID(-10010),
    COSTAFFID_PW_NOT_FOUND(-10011),
    INCORRECT_PASSWORD(-10012),

    EVALUATION_IS_NOT_REOPEN(-10020),
    CURRENT_HAVE_NO_PERIOD(-10021),
    STAFF_IS_NOT_IN_PERIOD(-10022),
    DO_NOT_HAVE_THIS_EVALUATION_ID(-10023),
    NO_LEADER(-10024),

    ARGS_NOT_VALID(-10030);


    private int code;

    ReturnCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
