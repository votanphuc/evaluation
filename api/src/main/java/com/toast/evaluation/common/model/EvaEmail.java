package com.toast.evaluation.common.model;

public interface EvaEmail {
    String getUsername();

    String getPassword();

    String getReceiver();

    String getSubject();

    String getContent();

    String getContentType();
}
