package com.toast.evaluation.common.exception;

public class InvalidTokenException extends EvaException {
    public InvalidTokenException(String msg) {
        super(msg);
    }
}
