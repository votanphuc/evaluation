package com.toast.evaluation.common.utils;

import com.toast.evaluation.common.constant.EvaConstant;
import com.toast.evaluation.common.exception.InvalidTokenException;
import com.toast.evaluation.model.Staff;
import io.jsonwebtoken.*;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtUtils {

    public String encode(Staff staff) {
        Map<String, Object> params = new HashMap<>();
        params.put("staffId", staff.getStaffId());
        params.put("coStaffId", staff.getCoStaffId());
        params.put("staffName", staff.getStaffName());
        params.put("email", staff.getEmail());
        params.put("role", staff.getRole());

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, EvaConstant.TIME_EXPIRE_TOKEN);
        Date expire = calendar.getTime();

        String compact = Jwts.builder().setClaims(params)
                .setIssuedAt(calendar.getTime())
                .setExpiration(expire)
                .setExpiration(calendar.getTime())
                .signWith(SignatureAlgorithm.HS512, EvaConstant.SECRET_KEY)
                .compact();

        return compact;
    }

    public Staff decode(String jwt) throws InvalidTokenException {
        Claims body = null;
        try {
            body = Jwts.parser()
                    .setSigningKey(EvaConstant.SECRET_KEY)
                    .parseClaimsJws(jwt).getBody();
        } catch (SignatureException ex) {
            throw new InvalidTokenException("Invalid signature");
        } catch (ExpiredJwtException ex) {
            throw new InvalidTokenException("Token expired");
        } catch (Exception ex) {
            throw new InvalidTokenException("Unknown EvaException");
        }

        int staffId = body.get("staffId", Integer.class);
        String coStaffId = body.get("coStaffId", String.class);
        String staffName = body.get("staffName", String.class);
        String email = body.get("email", String.class);
        String role = body.get("role", String.class);

        Staff staff = new Staff();
        staff.setStaffId(staffId);
        staff.setCoStaffId(coStaffId);
        staff.setStaffName(staffName);
        staff.setEmail(email);
        staff.setRole(role);

        return staff;
    }
}
