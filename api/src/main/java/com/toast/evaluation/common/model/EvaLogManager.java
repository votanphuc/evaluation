package com.toast.evaluation.common.model;

import org.apache.logging.log4j.LogManager;

public class EvaLogManager extends LogManager {

    private static final String PREFIX = "PEA: ";

    public static void log(String log) {
        System.out.println(PREFIX + log);
    }

    public static void log(String log, Object... objs) {
        System.out.print(PREFIX + log + ": ");
        for (Object obj : objs) {
            System.out.println(obj);
        }
    }

    public static void log(Object... logs) {
        for (Object log : logs) {
            System.out.println(PREFIX + log);
        }
    }
}
