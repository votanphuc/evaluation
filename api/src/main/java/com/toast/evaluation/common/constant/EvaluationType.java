package com.toast.evaluation.common.constant;

public enum EvaluationType {
    SELF_STAFF, SELF_LEAD, STAFF_LEAD
}
