package com.toast.evaluation.common.model;

import com.google.gson.Gson;
import com.toast.evaluation.common.constant.ReturnCode;

import java.io.Serializable;

public class EvaResponse<T> implements Serializable {
    private EvaHeader header = new EvaHeader();
    private T data;

    public EvaResponse() {
    }

    public EvaResponse(String msg, T data) {
        this.header = new EvaHeader(ReturnCode.SUCCESS.getCode(), msg);
        this.data = data;
    }

    public EvaResponse(int code, String msg, T data) {
        this.header = new EvaHeader(code, msg);
        this.data = data;
    }

    public EvaResponse(EvaHeader header, T data) {
        this.header = header;
        this.data = data;
    }

    public EvaResponse(T data) {
        this.header = new EvaHeader();
        this.data = data;
    }

    public EvaHeader getHeader() {
        return header;
    }

    public EvaResponse<T> setHeader(EvaHeader header) {
        this.header = header;
        return this;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}
