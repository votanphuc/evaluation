package com.toast.evaluation.common.constant;

public enum EvaluationStatus {
    INCOMPLETE, PENDING, REVIEWING, REVIEWED, APPROVE, REOPEN
}
