package com.toast.evaluation.common.model;

import com.toast.evaluation.common.constant.ReturnCode;

public class EvaHeader {
    private int code = ReturnCode.SUCCESS.getCode();
    private String msg = ReturnCode.SUCCESS.name();

    public EvaHeader() {
    }

    public EvaHeader(ReturnCode code) {
        this.code = code.getCode();
        this.msg = code.name();
    }

    public EvaHeader(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public EvaHeader(int code) {
        this.code = code;
    }

    public EvaHeader(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
