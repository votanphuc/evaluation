package com.toast.evaluation.common.constant;

public enum FinalResult {
    EXCELLENT, GOOD, ACCEPTABLE, NEED_IMPROVEMENT, UNSATISFACTORY;
}
