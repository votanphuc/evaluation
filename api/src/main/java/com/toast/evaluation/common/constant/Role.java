package com.toast.evaluation.common.constant;

public enum Role {
    STAFF, LEAD, ADMIN, CEO
}
