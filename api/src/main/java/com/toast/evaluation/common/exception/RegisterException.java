package com.toast.evaluation.common.exception;

import com.toast.evaluation.common.constant.ReturnCode;

public class RegisterException extends EvaException {

    public RegisterException(ReturnCode code) {
        super(code.name());
        setCode(code);
    }

}