package com.toast.evaluation.config;

import com.toast.evaluation.common.constant.ReturnCode;
import com.toast.evaluation.common.exception.EvaException;
import com.toast.evaluation.common.model.EvaHeader;
import com.toast.evaluation.common.model.EvaLogManager;
import com.toast.evaluation.common.model.EvaResponse;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalControllerAdvice {
    private static final Logger logger = EvaLogManager.getLogger(GlobalControllerAdvice.class);

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public EvaResponse unauthorized(Exception ex) {
        logger.warn("Unauthorized request: ", ex.getMessage());

        EvaHeader header = new EvaHeader(ReturnCode.UNAUTHORIZE);
        return new EvaResponse<>().setHeader(header);
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ResponseBody
    public EvaResponse forbidden(Exception ex) {
        logger.warn("Forbidden request: ", ex.getMessage());

        EvaHeader header = new EvaHeader(ReturnCode.FORBIDDEN);
        return new EvaResponse<>().setHeader(header);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public EvaResponse general(Exception ex) {
        logger.error("Internal server error request: ", ex);

        EvaHeader header = new EvaHeader(ReturnCode.INTERNAL_SERVER_ERROR);
        return new EvaResponse<>().setHeader(header);
    }

    @ExceptionHandler(EvaException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public EvaResponse sjException(EvaException ex) {
        EvaHeader header = new EvaHeader(ex.getCode());
        return new EvaResponse<>().setHeader(header);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public EvaResponse argsException(MethodArgumentNotValidException ex) {
        EvaHeader header = new EvaHeader(ReturnCode.ARGS_NOT_VALID);
        return new EvaResponse<>().setHeader(header);
    }

}
