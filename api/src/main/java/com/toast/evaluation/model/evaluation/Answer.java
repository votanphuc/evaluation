package com.toast.evaluation.model.evaluation;

import com.toast.evaluation.model.Staff;

public class Answer {
    private int answerId;
    private String selfAnswer;
    private String review;
    private String feedback;
    private Staff feedbacker;

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    public String getSelfAnswer() {
        return selfAnswer;
    }

    public void setSelfAnswer(String selfAnswer) {
        this.selfAnswer = selfAnswer;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Staff getFeedbacker() {
        return feedbacker;
    }

    public void setFeedbacker(Staff feedbacker) {
        this.feedbacker = feedbacker;
    }
}
