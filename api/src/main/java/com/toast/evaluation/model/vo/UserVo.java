package com.toast.evaluation.model.vo;

import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

public class UserVo {

    @ApiModelProperty(hidden = true)
    private String snsToken;
    private String coStaffId;
    private String password;

    public String getCoStaffId() {
        return coStaffId;
    }

    public void setCoStaffId(String coStaffId) {
        this.coStaffId = coStaffId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSnsToken() {
        return snsToken;
    }

    public void setSnsToken(String snsToken) {
        this.snsToken = snsToken;
    }

    public UserVo() {
        super();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserVo)) return false;
        UserVo userVo = (UserVo) o;
        return Objects.equals(getSnsToken(), userVo.getSnsToken()) &&
                Objects.equals(getCoStaffId(), userVo.getCoStaffId()) &&
                Objects.equals(getPassword(), userVo.getPassword());
    }

    @Override
    public int hashCode() {
        int result = coStaffId != null ? coStaffId.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
