package com.toast.evaluation.model.evaluation;

import java.util.Date;

public class ReopenEvaluation {
    private int evaId;
    private Date startDtReopen;
    private Date endDtReopen;

    public ReopenEvaluation() {
    }

    public ReopenEvaluation(int evaId, Date startDtReopen, Date endDtReopen) {
        this.evaId = evaId;
        this.startDtReopen = startDtReopen;
        this.endDtReopen = endDtReopen;
    }

    public int getEvaId() {
        return evaId;
    }

    public void setEvaId(int evaId) {
        this.evaId = evaId;
    }

    public Date getStartDtReopen() {
        return startDtReopen;
    }

    public void setStartDtReopen(Date startDtReopen) {
        this.startDtReopen = startDtReopen;
    }

    public Date getEndDtReopen() {
        return endDtReopen;
    }

    public void setEndDtReopen(Date endDtReopen) {
        this.endDtReopen = endDtReopen;
    }
}
