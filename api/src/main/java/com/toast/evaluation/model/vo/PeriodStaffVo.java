package com.toast.evaluation.model.vo;

import com.toast.evaluation.model.Period;

import java.util.List;

public class PeriodStaffVo {
    private Period period;
    private List<Integer> staffsId;

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public List<Integer> getStaffsId() {
        return staffsId;
    }

    public void setStaffsId(List<Integer> staffsId) {
        this.staffsId = staffsId;
    }
}
