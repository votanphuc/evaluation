package com.toast.evaluation.model;

import java.util.Date;

public class Period {
    private int periodId;
    private String periodName;
    private Date startDt; //YYYY-mm-dd201
    private Date endDtStaff;
    private Date endDtLead;
    private Date endDtAdmin;
    private Staff creator;

    public Staff getCreator() {
        return creator;
    }

    public void setCreator(Staff creator) {
        this.creator = creator;
    }

    public int getPeriodId() {
        return periodId;
    }

    public void setPeriodId(int periodId) {
        this.periodId = periodId;
    }

    public String getPeriodName() {
        return periodName;
    }

    public void setPeriodName(String periodName) {
        this.periodName = periodName;
    }

    public Date getStartDt() {
        return startDt;
    }

    public void setStartDt(Date startDt) {
        this.startDt = startDt;
    }

    public Date getEndDtStaff() {
        return endDtStaff;
    }

    public void setEndDtStaff(Date endDtStaff) {
        this.endDtStaff = endDtStaff;
    }

    public Date getEndDtLead() {
        return endDtLead;
    }

    public void setEndDtLead(Date endDtLead) {
        this.endDtLead = endDtLead;
    }

    public Date getEndDtAdmin() {
        return endDtAdmin;
    }

    public void setEndDtAdmin(Date endDtAdmin) {
        this.endDtAdmin = endDtAdmin;
    }
}
