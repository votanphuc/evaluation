package com.toast.evaluation.model.evaluation;

import com.toast.evaluation.common.constant.EvaluationStatus;
import com.toast.evaluation.common.constant.EvaluationType;
import com.toast.evaluation.common.constant.FinalResult;
import com.toast.evaluation.model.Period;
import com.toast.evaluation.model.Staff;

public class EvaluationHeader {
    private int evaluateId;
    private Period period;
    private EvaluationType evaType;
    private Staff staff;
    private EvaluationStatus status;
    private ReopenEvaluation reopen;
    private String overall;
    private FinalResult finalResult;


    public int getEvaluateId() {
        return evaluateId;
    }

    public void setEvaluateId(int evaluateId) {
        this.evaluateId = evaluateId;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public EvaluationType getEvaType() {
        return evaType;
    }

    public void setEvaType(String evaType) {
        this.evaType = EvaluationType.valueOf(evaType);
    }

    public void setEvaType(EvaluationType evaType) {
        this.evaType = evaType;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public EvaluationStatus getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = EvaluationStatus.valueOf(status);
    }

    public void setStatus(EvaluationStatus status) {
        this.status = status;
    }

    public ReopenEvaluation getReopen() {
        return reopen;
    }

    public void setReopen(ReopenEvaluation reopen) {
        this.reopen = reopen;
    }

    public String getOverall() {
        return overall;
    }

    public void setOverall(String overall) {
        this.overall = overall;
    }

    public FinalResult getFinalResult() {
        return finalResult;
    }

    public void setFinalResult(String finalResult) {
        this.finalResult = FinalResult.valueOf(finalResult);
    }

    public void setFinalResult(FinalResult finalResult) {
        this.finalResult = finalResult;
    }
}
