package com.toast.evaluation.model.evaluation;

import java.util.List;

public class Evaluation {
    private EvaluationHeader evaHeader;
    private List<Question> questions;

    public EvaluationHeader getEvaHeader() {
        return evaHeader;
    }

    public void setEvaHeader(EvaluationHeader evaHeader) {
        this.evaHeader = evaHeader;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
