package com.toast.evaluation.model;

import com.toast.evaluation.common.constant.Role;

public class Staff {
    private int staffId;
    private String coStaffId;
    private String staffName;
    private String position;
    private Team team;
    private Role role;
    private String email;

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public String getCoStaffId() {
        return coStaffId;
    }

    public void setCoStaffId(String coStaffId) {
        this.coStaffId = coStaffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = Role.valueOf(role);
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
