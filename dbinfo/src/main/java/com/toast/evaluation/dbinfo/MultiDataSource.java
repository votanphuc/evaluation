package com.toast.evaluation.dbinfo;

import com.toast.evaluation.dbinfo.constant.DatabaseType;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class MultiDataSource extends AbstractRoutingDataSource {

//    private static final Logger logger = SJLogsManager.getLogger(MultiDataSource.class);

    @Override
    protected Object determineCurrentLookupKey() {
        DatabaseType key = DatabaseType.MASTER;
        if (!TransactionSynchronizationManager.isSynchronizationActive() || TransactionSynchronizationManager.isCurrentTransactionReadOnly()) {
            key = DatabaseType.SLAVE;
        }

//        logger.debug("Found key: " + key);
        return key;
    }
}