package com.toast.evaluation.dbinfo.constant;

public enum DatabaseType {
    MASTER("master"), SLAVE("slave");

    private String prefix;

    DatabaseType(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }
}
