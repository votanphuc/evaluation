package com.toast.evaluation.dbinfo;

import com.toast.evaluation.dbinfo.constant.DatabaseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
//@PropertySource("classpath:dbinfo-${spring.profiles.active}.properties")
@PropertySource("classpath:dbinfo-alpha.properties")
public class DatasourceConfig {

//    private static final Logger logger = SJLogsManager.getLogger(DatasourceConfig.class);

    @Autowired
    private Environment env;

    @Bean(name = "dataSource")
    public DataSource getDataSource() {
//        logger.info("=======================================================");
        System.out.printf("===================DB-Info==========================");
        DataSource masterDataSource = getMasterDataSource();
        DataSource slaveDataSource = getSlaveDataSource();
        System.out.printf("===================DB-Info==========================");
//        logger.info("=======================================================");

        MultiDataSource dataSource = new MultiDataSource();

        Map<Object, Object> resolvedDataSources = new HashMap<>();

        resolvedDataSources.put(DatabaseType.MASTER, masterDataSource);
        resolvedDataSources.put(DatabaseType.SLAVE, slaveDataSource);
        dataSource.setTargetDataSources(resolvedDataSources);
        dataSource.afterPropertiesSet();

        return new LazyConnectionDataSourceProxy(dataSource);
    }

    @Bean(name = "transactionManager")
    public DataSourceTransactionManager transactionManager() {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(getDataSource());
        return transactionManager;
    }

    private DataSource getMasterDataSource() {
        return getDataSource(DatabaseType.MASTER.getPrefix());
    }

    private DataSource getSlaveDataSource() {
        return getDataSource(DatabaseType.SLAVE.getPrefix());
    }

    private DataSource getDataSource(String prefix) {
        String driverClassName = env.getProperty(prefix + ".datasource.driver-class-name");
        String url = env.getProperty(prefix + ".datasource.url");
        String username = env.getProperty(prefix + ".datasource.username");
        String password = env.getProperty(prefix + ".datasource.password");

//        logger.info("Datasource dbinfo [{}]: username={}, password={}, url={}", new Object[]{prefix, username, password, url});
        System.out.println("Prefix=" + prefix + " Username=" + username + " Password=" + password + " Url=" + url);

        DataSourceBuilder factory = DataSourceBuilder
                .create()
                .driverClassName(driverClassName)
                .url(url)
                .username(username)
                .password(password);
        return factory.build();
    }
}
