/*
 Navicat Premium Data Transfer

 Source Server         : DB Master
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : 10.78.249.11:3306
 Source Schema         : evaluation

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 28/12/2018 14:16:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for answers
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers`  (
  `answerId` int(11) NOT NULL AUTO_INCREMENT,
  `selfAnswer` json NULL,
  `review` json NULL,
  `feedback` json NULL,
  `feedbackerId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`answerId`) USING BTREE,
  INDEX `feedbackerId`(`feedbackerId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 227 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of answers
-- ----------------------------
INSERT INTO `answers` VALUES (1, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Feedback\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (2, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Feedback\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (3, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Feedback\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (4, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Feedback\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (5, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Feedback\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (7, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (8, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (27, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (28, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (29, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (30, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (31, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (32, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (33, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (34, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (35, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (36, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (37, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (38, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (39, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (40, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (41, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (42, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (43, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (44, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (45, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (46, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (47, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (48, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (49, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (50, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (51, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (52, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (53, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (54, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (55, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (56, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (57, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (58, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (59, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (60, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (61, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (62, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (63, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (64, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (65, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (66, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (67, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (68, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (69, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (70, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (71, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (72, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (73, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (74, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (75, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (76, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (78, '[{\"colA\": \"Maxime laboriosam accusamus inventore consectetur rerum eligendi error autem in? \", \"colB\": \"Lorem ipsum dolor, sit amet consectetur adipisicing elit.\", \"colC\": \"Maxime laboriosam accusamus inventore consectetur rerum eligendi error autem in? \", \"colD\": \"Lorem ipsum dolor, sit amet consectetur adipisicing elit.\"}, {\"colA\": \"Lorem ipsum dolor, sit amet consectetur adipisicing elit.\", \"colB\": \"Maxime laboriosam accusamus inventore consectetur rerum eligendi error autem in? \", \"colC\": \"Maxime laboriosam accusamus inventore consectetur rerum eligendi error autem in? \", \"colD\": \"Completed on time\"}]', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (79, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (80, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (81, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (82, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (83, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (84, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (85, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (86, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sint ad tenetur aliquam laudantium. Nihil architecto vero inventore voluptatem saepe modi similique, in fuga nobis molestias. Est magni repellendus aliquid!\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (87, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (88, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (89, '[{\"colA\": \"Maxime laboriosam accusamus inventore consectetur rerum eligendi error autem in? \", \"colB\": \"Lorem ipsum dolor, sit amet consectetur adipisicing elit.\", \"colC\": \"Maxime laboriosam accusamus inventore consectetur rerum eligendi error autem in? \", \"colD\": \"Lorem ipsum dolor, sit amet consectetur adipisicing elit.\"}, {\"colA\": \"Lorem ipsum dolor, sit amet consectetur adipisicing elit.\", \"colB\": \"Maxime laboriosam accusamus inventore consectetur rerum eligendi error autem in? \", \"colC\": \"Maxime laboriosam accusamus inventore consectetur rerum eligendi error autem in? \", \"colD\": \"Completed on time\"}]', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (90, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (91, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (92, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (93, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (94, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (95, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (96, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (97, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sint ad tenetur aliquam laudantium. Nihil architecto vero inventore voluptatem saepe modi similique, in fuga nobis molestias. Est magni repellendus aliquid!\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (98, '[]', '{}', NULL, 1);
INSERT INTO `answers` VALUES (99, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (100, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (101, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (102, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (103, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (104, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (105, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (106, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sint ad tenetur aliquam laudantium. Nihil architecto vero inventore voluptatem saepe modi similique, in fuga nobis molestias. Est magni repellendus aliquid!\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (107, '[{\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', '{}', NULL, 1);
INSERT INTO `answers` VALUES (108, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (109, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (110, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (111, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (112, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (113, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (114, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (115, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sint ad tenetur aliquam laudantium. Nihil architecto vero inventore voluptatem saepe modi similique, in fuga nobis molestias. Est magni repellendus aliquid!\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (116, '[{\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', '{}', NULL, 1);
INSERT INTO `answers` VALUES (117, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (118, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (119, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (120, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (121, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (122, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (123, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (124, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sint ad tenetur aliquam laudantium. Nihil architecto vero inventore voluptatem saepe modi similique, in fuga nobis molestias. Est magni repellendus aliquid!\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (125, '[{\"colA\": \"dsadsa\", \"colB\": \"sadasd\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"asd\", \"colB\": \"\", \"colC\": \"asdsa\", \"colD\": \"asd\"}, {\"colA\": \"asdas\", \"colB\": \"asd\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"asdsa\", \"colD\": \"asdas\"}, {\"colA\": \"asd\", \"colB\": \"asdsa\", \"colC\": \"\", \"colD\": \"assd\"}]', '{}', NULL, 1);
INSERT INTO `answers` VALUES (126, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (127, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (128, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (129, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (130, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (131, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (132, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (133, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sint ad tenetur aliquam laudantium. Nihil architecto vero inventore voluptatem saepe modi similique, in fuga nobis molestias. Est magni repellendus aliquid!\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (134, '[{\"colA\": \"dasdsa\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"sadsad\", \"colC\": \"sada\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"dasd\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', '{}', NULL, 1);
INSERT INTO `answers` VALUES (135, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (136, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (137, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (138, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (139, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (140, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (141, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (142, '\"\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (143, '[{\"colA\": \"sadsad\", \"colB\": \"dsadsad\", \"colC\": \"asdsad\", \"colD\": \"dsad\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', '{}', NULL, 1);
INSERT INTO `answers` VALUES (144, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (145, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (146, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (147, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (148, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (149, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (150, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (151, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sint ad tenetur aliquam laudantium. Nihil architecto vero inventore voluptatem saepe modi similique, in fuga nobis molestias. Est magni repellendus aliquid!\"', '{}', NULL, 1);
INSERT INTO `answers` VALUES (152, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (153, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (154, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (155, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (156, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (157, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (158, '[{\"colA\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\", \"colB\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\", \"colC\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\", \"colD\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (159, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (160, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (161, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (162, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (163, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Acceptable\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (164, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Acceptable\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (165, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Acceptable\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (166, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sint ad tenetur aliquam laudantium. Nihil architecto vero inventore voluptatem saepe modi similique, in fuga nobis molestias. Est magni repellendus aliquid!\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (167, '[{\"colA\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\", \"colB\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\", \"colC\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\", \"colD\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (168, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (169, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (170, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (171, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (172, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Acceptable\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (173, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Acceptable\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (174, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Acceptable\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (175, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sint ad tenetur aliquam laudantium. Nihil architecto vero inventore voluptatem saepe modi similique, in fuga nobis molestias. Est magni repellendus aliquid!\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (176, '[{\"colA\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\", \"colB\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\", \"colC\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\", \"colD\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (177, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (178, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (179, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (180, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (181, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Acceptable\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (182, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Acceptable\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (183, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"type\": \"Acceptable\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (184, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sint ad tenetur aliquam laudantium. Nihil architecto vero inventore voluptatem saepe modi similique, in fuga nobis molestias. Est magni repellendus aliquid!\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (185, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (186, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (187, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (188, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (189, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (190, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (191, '{\"3123\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (192, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (193, '{\"ddsdsdsds\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (194, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (195, '{\"ddsdsdsds\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (196, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (197, '{\"ddsdsdsds\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (198, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (199, '{\"ddsdsdsds\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (200, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (201, '{\"ddsdsdsds\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (202, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (203, '{\"ddsdsdsds\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (204, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (205, '{\"ddsdsdsds\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (206, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (207, '{\"ddsdsdsds\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (208, '{\"PEA\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (209, '{\"qweqwe\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (210, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (211, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (212, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (213, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (214, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (215, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (216, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (217, '{\"SelfAnswer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', '{\"Reviewer\": \"This section is to be filled by both the reviewee and the reviewer.\"}', 1);
INSERT INTO `answers` VALUES (218, '[{\"colA\": \"A1\", \"colB\": \"B1\", \"colC\": \"C1\", \"colD\": \"D1\"}, {\"colA\": \"A2\", \"colB\": \"B2\", \"colC\": \"C2\", \"colD\": \"D2\"}, {\"colA\": \"A3\", \"colB\": \"B3\", \"colC\": \"C3\", \"colD\": \"D3\"}, {\"colA\": \"A4\", \"colB\": \"B4\", \"colC\": \"C4\", \"colD\": \"D4\"}, {\"colA\": \"a5\", \"colB\": \"b5\", \"colC\": \"c5\", \"colD\": \"d5\"}]', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (219, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (220, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (221, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (222, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (223, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (224, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (225, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto natus qui unde tenetur saepe, magnam veritatis ex? Dolorem debitis magni amet beatae ipsam obcaecati porro? Asperiores reprehenderit voluptatibus id placeat.\"', '{\"content\": \"\"}', NULL, 1);
INSERT INTO `answers` VALUES (226, '\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sint ad tenetur aliquam laudantium. Nihil architecto vero inventore voluptatem saepe modi similique, in fuga nobis molestias. Est magni repellendus aliquid!\"', '{\"content\": \"\"}', NULL, 1);

-- ----------------------------
-- Table structure for eva_ques_ans
-- ----------------------------
DROP TABLE IF EXISTS `eva_ques_ans`;
CREATE TABLE `eva_ques_ans`  (
  `answerId` int(11) NOT NULL AUTO_INCREMENT,
  `evaluateId` int(11) NULL DEFAULT NULL,
  `questionId` int(11) NULL DEFAULT NULL,
  `selfAnswer` json NULL,
  `review` json NULL,
  `feedback` json NULL,
  `feedbackerId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`answerId`) USING BTREE,
  INDEX `evaluateId`(`evaluateId`) USING BTREE,
  INDEX `answerId`(`answerId`) USING BTREE,
  INDEX `questionId`(`questionId`) USING BTREE,
  INDEX `ansfeedbackerId`(`feedbackerId`) USING BTREE,
  CONSTRAINT `ansfeedbackerId` FOREIGN KEY (`feedbackerId`) REFERENCES `staffs` (`staffId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `evaluateId` FOREIGN KEY (`evaluateId`) REFERENCES `evaluates` (`evaluateId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `questionId` FOREIGN KEY (`questionId`) REFERENCES `questions` (`questionId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 687 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of eva_ques_ans
-- ----------------------------
INSERT INTO `eva_ques_ans` VALUES (511, 135, 13, '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"dsadsadsa\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (512, 135, 14, '{\"content\": \"\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (513, 135, 15, '{\"content\": \"\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (514, 135, 16, '{\"content\": \"\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (520, 137, 14, '{\"content\": \"\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (521, 137, 15, '{\"content\": \"\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (522, 137, 16, '{\"content\": \"\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (527, 139, 13, '{\"type\": \"Exceeded Expectations\", \"content\": \"Cau 1\"}', '\"\"', '\"sdasds\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (528, 139, 14, '{\"type\": \"Exceeded Expectations\", \"content\": \"Cau 2\"}', '\"\"', '\"adsad\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (529, 139, 15, '{\"type\": \"Acceptable\", \"content\": \"Cau 3\"}', '\"\"', '\"sadsad\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (530, 139, 16, '{\"type\": \"Need to improve\", \"content\": \"Cau 4\"}', '\"\"', '\"asdsadsad\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (540, 141, 1, '[{\"colA\": \"asdsad\", \"colB\": \"asdad\", \"colC\": \"asdsa\", \"colD\": \"asdasd\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', '{\"type\": \"Exceeded Expectations\", \"content\": \"maintain\"}', '\"moment\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (541, 141, 2, '\"section 2 - cau 1\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"fsdfsdf\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (542, 141, 3, '\"sadsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sdfsf\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (543, 141, 4, '\"asdsadsad\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"dsfsdfdsf\"}', '\"sadsad\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (544, 141, 5, '\"asdsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sdfsdfsf\"}', '\"sdasadsad\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (545, 141, 6, '\"sdsada\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"sdfsdfdsf\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (546, 141, 7, '\"asdasd\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sdfssdf\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (547, 141, 8, '\"asdsadsa\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sdfsdfsf\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (548, 141, 9, '\"adsadasd\"', '{\"other\": \"sdfsdfs\", \"expectation\": \"sdfsdf\", \"improvement\": \"sdsadsads\", \"strongPoints\": \"dsfsdfsdf\"}', '{\"overall\": \"\", \"finalResult\": \"\"}', NULL);
INSERT INTO `eva_ques_ans` VALUES (574, 147, 1, '{\"tableA\": [{\"colA\": \"sdsadsa\", \"colB\": \"N?m nay\", \"colC\": \"sadsad\", \"colD\": \"sadsad\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}], \"tableB\": [{\"colA\": \"sadsa\", \"colB\": \"??\", \"colC\": \"Ã????????????????Ã???????????????Ã??????????????Ã?????????????Ã????????????Ã???????????Ã??????????Ã?????????Ã????????Ã???????Ã??????Ã?????Ã????Ã???Ã??Ã?Â¡dsad\", \"colD\": \"sd\"}]}', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"sÃ??????Ã?????Ã????Ã???Ã??Ã?Â¡dsadad\"}', '\"sadsadsa\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (575, 147, 2, '\"Ã????????????????Ã???????????????Ã??????????????Ã?????????????Ã????????????Ã???????????Ã??????????Ã?????????Ã????????Ã???????Ã??????Ã?????Ã????Ã???Ã??Ã?Â¡d\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"dsadsada\"}', '\"sadsadsad\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (576, 147, 3, '\"sdsad\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"adsadsa\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (577, 147, 10, '\"adsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sadsad\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (578, 147, 11, '\"sdsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sadsad\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (579, 147, 6, '\"sdsadasd\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"sadsad\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (580, 147, 7, '\"sadsadsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sadas?\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (581, 147, 8, '\"Ã???????????????Ã??????????????Ã?????????????Ã????????????Ã???????????Ã??????????Ã?????????Ã????????Ã???????Ã??????Ã?????Ã????Ã???Ã??Ã?Â¡dsadsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"Ã????Ã???Ã??Ã?Â¡dasda\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (582, 147, 12, '\"ssdsadad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"Ã????Ã???Ã??Ã?Â¡dasd\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (583, 147, 9, '\"4 nek\"', '{\"other\": \"sadsad\", \"expectation\": \"Ã???????Ã??????Ã?????Ã????Ã???Ã??Ã?Â¡dsadsa\", \"improvement\": \"Ã???????Ã??????Ã?????Ã????Ã???Ã??Ã?Â¡dsada\", \"strongPoints\": \"sdad\"}', '{\"overall\": \"\", \"finalResult\": \"\", \"staffAppraisal\": [{\"colA\": \"sdasdsa\", \"colB\": \"Ã??Ã?Â¡dsad\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}], \"staffAppraisalComment\": \"\"}', NULL);
INSERT INTO `eva_ques_ans` VALUES (584, 148, 1, '[{\"colA\": \"dadsad\", \"colB\": \"adsad\", \"colC\": \"asdsad\", \"colD\": \"asdsad\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"sdsadsadsa\"}', NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (585, 148, 2, '\"asdsad\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"dsad\"}', NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (586, 148, 3, '\"asdsad\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"asdsadsad\"}', NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (587, 148, 4, '\"asdsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sadasdsad\"}', NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (588, 148, 5, '\"asdad\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"asdsadsad\"}', NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (589, 148, 6, '\"sdad\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"asdsadsad\"}', NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (590, 148, 7, '\"asdsad\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"assdsadsad\"}', NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (591, 148, 8, '\"asdasda\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"sasdsadad\"}', NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (592, 148, 9, '\"sdasdsad\"', '{\"other\": \"asdsad\", \"expectation\": \"asdasdsad\", \"improvement\": \"saasdsad\", \"strongPoints\": \"sadasdasd\"}', '{}', NULL);
INSERT INTO `eva_ques_ans` VALUES (603, 150, 13, '{\"type\": \"Exceeded Expectations\", \"content\": \"sadasdsad\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (604, 150, 14, '{\"type\": \"Exceeded Expectations\", \"content\": \"sadsad\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (605, 150, 15, '{\"type\": \"Exceeded Expectations\", \"content\": \"sadasd\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (606, 150, 16, '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"sadsad\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (607, 151, 1, '[{\"colA\": \"asdsad\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"dsadasd\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"sdsadsad\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"Kang review Thuong nek\"}', '\"Khoong co j de feedback\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (608, 151, 2, '\"dasdasd\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"sdsadasd\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (609, 151, 3, '\"sdsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"adasd\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (610, 151, 4, '\"asdasdsa\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"asdsadsad\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (611, 151, 5, '\"asdsad\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"adasdsd\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (612, 151, 6, '\"asdasdsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"dasd\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (613, 151, 7, '\"assdsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"adasdad\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (614, 151, 8, '\"assdsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sadsadasd\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (615, 151, 9, '\"dasdad\"', '{\"other\": \"asdasd\", \"expectation\": \"assdasd\", \"improvement\": \"assdasd\", \"strongPoints\": \"dasdas\"}', '{\"overall\": \"dadasd\", \"finalResult\": \"asdsadsad\"}', NULL);
INSERT INTO `eva_ques_ans` VALUES (616, 152, 13, '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"sdsad\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (617, 152, 14, '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"asdasdad\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (618, 152, 15, '{\"type\": \"Exceeded Expectations\", \"content\": \"sadasdsd\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (619, 152, 16, '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"asdsadasd\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (620, 153, 1, '{\"tableA\": [{\"colA\": \"sdsadd\", \"colB\": \"sadasd\", \"colC\": \"asdasd\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}], \"tableB\": [{\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]}', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"sdasdsad\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (621, 153, 2, '\"asdasd\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sdasd\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (622, 153, 3, '\"adasd\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"adsadad\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (623, 153, 10, '\"asdsa\"', '{\"type\": \"Acceptable\", \"content\": \"sadsad\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (624, 153, 11, '\"asdsad\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"Ã?Â¡dsad\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (625, 153, 6, '\"assdsad\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sdsadasd\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (626, 153, 7, '\"asdasd\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sdads\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (627, 153, 8, '\"adad\"', '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"sadsad\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (628, 153, 12, '\"adasdasd\"', '{\"type\": \"Exceeded Expectations\", \"content\": \"sdasdasd\"}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (629, 153, 9, '\"sadsad\"', '{\"other\": \"adsad\", \"expectation\": \"sadsad\", \"improvement\": \"Ã?Â¡dsad\", \"strongPoints\": \"sadsad\"}', '{\"staffAppraisal\": []}', NULL);
INSERT INTO `eva_ques_ans` VALUES (630, 154, 13, '{\"type\": \"Exceeded Expectations\", \"content\": \"dadsad\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (631, 154, 14, '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"adsad\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (632, 154, 15, '{\"type\": \"Acceptable\", \"content\": \"asdasdad\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (633, 154, 16, '{\"type\": \"Need to improve\", \"content\": \"adsadsadad\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (634, 155, 1, '[{\"colA\": \"asdads\", \"colB\": \"asdasd\", \"colC\": \"asdasd\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"asdasd\"}, {\"colA\": \"\", \"colB\": \"asdadasd\", \"colC\": \"asdsd\", \"colD\": \"\"}, {\"colA\": \"asdasdasd\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"adasda\"}]', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (635, 155, 2, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (636, 155, 3, '\"Ã???????????????????????????Ã??????????????????????????Ã?????????????????????????Ã????????????????????????Ã???????????????????????Ã??????????????????????Ã?????????????????????Ã????????????????????Ã???????????????????Ã??????????????????Ã?????????????????Ã????????????????Ã???????????????Ã??????????????Ã?????????????Ã????????????Ã???????????Ã??????????Ã?????????Ã????????Ã???????Ã??????Ã?????Ã????Ã???Ã??Ã?Â¡dasdasdas adasdasdsadasd Ã¡dasdasdsad  \"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (637, 155, 4, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (638, 155, 5, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (639, 155, 6, '\"[\\\"3\\\", \\\"3\\\", \\\"2\\\"]\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (640, 155, 7, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (641, 155, 8, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (642, 155, 9, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (643, 156, 13, '{\"type\": \"Need to improve\", \"content\": \"Ã???Ã??Ã?Â¡dasdsad\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (644, 156, 14, '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (645, 156, 15, '{\"content\": \"\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (646, 156, 16, '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"Tr?n Qu?c To?n \\nTr?n Ngh?a YÃªn \"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (647, 157, 1, '[{\"colA\": \"asdasd\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"sadasdas\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (648, 157, 2, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (649, 157, 3, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (650, 157, 4, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (651, 157, 5, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (652, 157, 6, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (653, 157, 7, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (654, 157, 8, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (655, 157, 9, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (656, 158, 13, '{\"content\": \"sadasdsa\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (657, 158, 14, '{\"content\": \"asdsad\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (658, 158, 15, '{\"content\": \"asdasd\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (659, 158, 16, '{\"content\": \"asdasd\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (660, 159, 1, '[{\"colA\": \"Find new customers\", \"colB\": \"Find some new customers from other countries that can improve the product quality\", \"colC\": \"really good, have many customers come our store and bought many things\", \"colD\": \"on time\\nquickly\\nbefore dead line\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}, {\"colA\": \"\", \"colB\": \"\", \"colC\": \"\", \"colD\": \"\"}]', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (661, 159, 2, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (662, 159, 3, '\"Good and have a good relationship with all memebers\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (663, 159, 4, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (664, 159, 5, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (665, 159, 6, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (666, 159, 7, '\"\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (667, 159, 8, '\"Yeah, I have a good teamwork attitude\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (668, 159, 9, '\"Everything is good but also feel uncomfortable\"', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (669, 160, 13, '{\"type\": \"Need to improve\", \"content\": \"Need to improve\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (670, 160, 14, '{\"type\": \"Need to improve\", \"content\": \"Need to improve\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (671, 160, 15, '{\"type\": \"Need to improve\", \"content\": \"Need to improve\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (672, 160, 16, '{\"type\": \"Need to improve\", \"content\": \"Need to improve\"}', '\"\"', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (673, 161, 13, '{\"type\": \"Exceeded Expectations\", \"content\": \"\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (674, 161, 14, '{\"type\": \"Acceptable\", \"content\": \"Have nohthing to write here\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (675, 161, 15, '{\"type\": \"Significantly Exceeded Expectations\", \"content\": \"\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (676, 161, 16, '{\"type\": \"Need to improve\", \"content\": \"\"}', NULL, NULL, NULL);
INSERT INTO `eva_ques_ans` VALUES (677, 162, 1, '{\"tableA\": [{\"colA\": \"This section is to be filled\", \"colB\": \"by both the reviewee\", \"colC\": \"and the reviewer\", \"colD\": \"Ok\"}, {\"colA\": \"This section is to be filled\", \"colB\": \"by both the reviewee\", \"colC\": \"and the reviewer\", \"colD\": \"Ok\"}, {\"colA\": \"This section is to be filled\", \"colB\": \"by both the reviewee\", \"colC\": \"and the reviewer\", \"colD\": \"Ok\"}], \"tableB\": [{\"colA\": \"Ok\", \"colB\": \"Ok\", \"colC\": \"Ok\", \"colD\": \"Ok\"}]}', '{}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (678, 162, 2, '\"Demonstrate knowledge of technical\"', '{}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (679, 162, 3, '\"Communication\"', '{}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (680, 162, 10, '\"Decision making\"', '{}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (681, 162, 11, '\"Coaching and developing\"', '{}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (682, 162, 6, '\"Diligence and compliance\"', '{}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (683, 162, 7, '\"Accountablility\"', '{}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (684, 162, 8, '\"Teamwork\"', '{}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (685, 162, 12, '\"Ambition for company\\n\"', '{}', '\"\"', NULL);
INSERT INTO `eva_ques_ans` VALUES (686, 162, 9, '\"Good good good\"', '{}', '\"\"', NULL);

-- ----------------------------
-- Table structure for evaluates
-- ----------------------------
DROP TABLE IF EXISTS `evaluates`;
CREATE TABLE `evaluates`  (
  `evaluateId` int(11) NOT NULL AUTO_INCREMENT,
  `periodId` int(11) NULL DEFAULT NULL,
  `evaType` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `staffId` int(11) NULL DEFAULT NULL,
  `evaStatus` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `overall` json NULL,
  `finalResult` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `deleted` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`evaluateId`) USING BTREE,
  INDEX `evatypeId`(`evaType`) USING BTREE,
  INDEX `evastaffId`(`staffId`) USING BTREE,
  INDEX `periodId`(`periodId`) USING BTREE,
  INDEX `statusId`(`evaStatus`) USING BTREE,
  CONSTRAINT `evastaffId` FOREIGN KEY (`staffId`) REFERENCES `staffs` (`staffId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `periodId` FOREIGN KEY (`periodId`) REFERENCES `periods` (`periodId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 163 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of evaluates
-- ----------------------------
INSERT INTO `evaluates` VALUES (135, 4, 'STAFF_LEAD', 100, 'PENDING', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (137, 4, 'STAFF_LEAD', 5, 'REOPEN', '\"\"', 'EXCELLENT', NULL);
INSERT INTO `evaluates` VALUES (139, 4, 'STAFF_LEAD', 4, 'REOPEN', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (141, 4, 'SELF_STAFF', 4, 'APPROVE', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (147, 4, 'SELF_LEAD', 2, 'APPROVE', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (148, 4, 'SELF_STAFF', 3, 'REVIEWED', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (150, 4, 'STAFF_LEAD', 2, 'PENDING', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (151, 4, 'SELF_STAFF', 101, 'REVIEWED', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (152, 4, 'STAFF_LEAD', 101, 'PENDING', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (153, 4, 'SELF_LEAD', 102, 'REVIEWED', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (154, 4, 'STAFF_LEAD', 102, 'APPROVE', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (155, 104, 'SELF_STAFF', 104, 'INCOMPLETE', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (156, 104, 'STAFF_LEAD', 104, 'INCOMPLETE', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (157, 104, 'SELF_STAFF', 5, 'INCOMPLETE', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (158, 104, 'STAFF_LEAD', 5, 'INCOMPLETE', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (159, 104, 'SELF_STAFF', 3, 'INCOMPLETE', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (160, 104, 'STAFF_LEAD', 2, 'PENDING', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (161, 104, 'STAFF_LEAD', 3, 'INCOMPLETE', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);
INSERT INTO `evaluates` VALUES (162, 104, 'SELF_LEAD', 2, 'PENDING', '{\"comment\": \"He quite good in cot\"}', 'GOOD', NULL);

-- ----------------------------
-- Table structure for periods
-- ----------------------------
DROP TABLE IF EXISTS `periods`;
CREATE TABLE `periods`  (
  `periodId` int(11) NOT NULL AUTO_INCREMENT,
  `periodName` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `startDt` date NULL DEFAULT NULL,
  `endDtStaff` date NULL DEFAULT NULL,
  `endDtLead` date NULL DEFAULT NULL,
  `endDtAdmin` date NULL DEFAULT NULL,
  `creatorId` int(11) NOT NULL,
  `deleted` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`periodId`) USING BTREE,
  INDEX `creatorId`(`creatorId`) USING BTREE,
  CONSTRAINT `creatorId` FOREIGN KEY (`creatorId`) REFERENCES `staffs` (`staffId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of periods
-- ----------------------------
INSERT INTO `periods` VALUES (1, 'p', '2018-03-03', '2018-03-03', '2018-03-03', '2018-03-03', 2, -1);
INSERT INTO `periods` VALUES (4, 'Dot II - 2018', '2018-12-14', '2018-12-16', '2018-12-17', '2018-12-19', 100, 0);
INSERT INTO `periods` VALUES (102, 'Nanan', '2018-12-02', '2018-12-03', '2018-12-04', '2018-12-05', 100, -1);
INSERT INTO `periods` VALUES (103, 'Dot III - 2018', '2018-12-19', '2018-12-20', '2018-12-21', '2018-12-22', 100, -1);
INSERT INTO `periods` VALUES (104, 'Dot III - 2018', '2018-12-18', '2018-12-19', '2018-12-20', '2018-12-23', 100, NULL);

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`  (
  `questionId` int(11) NOT NULL AUTO_INCREMENT,
  `question` json NULL,
  PRIMARY KEY (`questionId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES (1, '{\"title\": \"null\", \"content\": \"null\"}');
INSERT INTO `questions` VALUES (2, '{\"title\": \"Functional Expertise\", \"content\": \"Demonstrate knowledge of technical / functional / business aspects of work required to successfully perform job responsibilities\"}');
INSERT INTO `questions` VALUES (3, '{\"title\": \"Communication\", \"content\": \"Demonstrate ability to understand others and express ideas in concise, convincing and efffective manner. \"}');
INSERT INTO `questions` VALUES (4, '{\"title\": \"Problem solving\", \"content\": \"Demonstrate ability to recognizes issues, and determines actions needed for resolving.\"}');
INSERT INTO `questions` VALUES (5, '{\"title\": \"Adaptability\", \"content\": \"Demonstrate ability to work effectively in a variety of situations, and with various individuals or groups.\"}');
INSERT INTO `questions` VALUES (6, '{\"title\": \"Diligence and compliance\", \"content\": \"Demonstrate the ability to complete all assigned responsibilities on time, attendance/punctuality , etc & compliance with company policies, procedures and rules.\"}');
INSERT INTO `questions` VALUES (7, '{\"title\": \"Accountablility\", \"content\": \"Demonstrates a high level of ownership and commitment to achieving results (including responsibility, effort to complete assigned duties)\"}');
INSERT INTO `questions` VALUES (8, '{\"title\": \"Teamwork\", \"content\": \"Demonstrate a strong willingness to work closely with colleagues toward a common goal (task force, special issue).  \"}');
INSERT INTO `questions` VALUES (9, '{\"title\": \"null\", \"content\": \"null\"}');
INSERT INTO `questions` VALUES (10, '{\"title\": \"Decision making\", \"content\": \"Assesses the importance, urgency and risk associated with each situation and takes actions which are timely and in the best interests of the organization\"}');
INSERT INTO `questions` VALUES (11, '{\"title\": \"Coaching and developing\", \"content\": \"Create an environment supportive of learning, training and development that enhance the professional knowledge and capability of others\"}');
INSERT INTO `questions` VALUES (12, '{\"title\": \"Ambition for company\", \"content\": \"Ambition for the company\'s substainable growth/ success\"}');
INSERT INTO `questions` VALUES (13, '{\"title\": \"Orientation and instruction\", \"content\": \"In the review period, in regard to work task, how well Mr./ Ms. [name] provide appropriate orientation and instruction when assigning task to team members?\"}');
INSERT INTO `questions` VALUES (14, '{\"title\": \"Coaching and developing subordinate\", \"content\": \"In the review period, how well did Mr/ Ms. [name] perform his/ her coaching/ monitoring to develope the subordinates\' capability? \"}');
INSERT INTO `questions` VALUES (15, '{\"title\": \"Diligence and compliance\", \"content\": \"In the review period, how well did Mr./ Ms. [name]’s enthusiastic constructing effort positively influence team members and performances align with the company’s policies to be a model for his/ her staffs?\"}');
INSERT INTO `questions` VALUES (16, '{\"title\": \"Functional expertise\", \"content\": \"In your opinion, does Mr./ Ms. [name] possess qualified knowledge and skills to handle his/her current position?\"}');

-- ----------------------------
-- Table structure for reopen
-- ----------------------------
DROP TABLE IF EXISTS `reopen`;
CREATE TABLE `reopen`  (
  `evaluateId` int(11) NOT NULL,
  `startDtReopen` datetime(0) NULL DEFAULT NULL,
  `endDtReopen` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`evaluateId`) USING BTREE,
  INDEX `reopenevaluateId`(`evaluateId`) USING BTREE,
  CONSTRAINT `reopenevaluateId` FOREIGN KEY (`evaluateId`) REFERENCES `evaluates` (`evaluateId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of reopen
-- ----------------------------
INSERT INTO `reopen` VALUES (137, '2018-01-19 00:12:00', '2018-01-20 00:12:00');
INSERT INTO `reopen` VALUES (139, '2018-01-19 00:12:00', '2018-01-23 00:12:00');
INSERT INTO `reopen` VALUES (141, '2018-12-19 00:00:00', '2018-12-20 00:00:00');

-- ----------------------------
-- Table structure for staff_period
-- ----------------------------
DROP TABLE IF EXISTS `staff_period`;
CREATE TABLE `staff_period`  (
  `spId` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NULL DEFAULT NULL,
  `periodId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`spId`) USING BTREE,
  INDEX `staffId`(`staffId`) USING BTREE,
  INDEX ` periodId`(`periodId`) USING BTREE,
  CONSTRAINT ` periodId` FOREIGN KEY (`periodId`) REFERENCES `periods` (`periodId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `staffId` FOREIGN KEY (`staffId`) REFERENCES `staffs` (`staffId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 602 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of staff_period
-- ----------------------------
INSERT INTO `staff_period` VALUES (571, 2, 4);
INSERT INTO `staff_period` VALUES (572, 3, 4);
INSERT INTO `staff_period` VALUES (573, 4, 4);
INSERT INTO `staff_period` VALUES (574, 5, 4);
INSERT INTO `staff_period` VALUES (575, 100, 4);
INSERT INTO `staff_period` VALUES (576, 101, 4);
INSERT INTO `staff_period` VALUES (577, 102, 4);
INSERT INTO `staff_period` VALUES (578, 103, 4);
INSERT INTO `staff_period` VALUES (594, 2, 104);
INSERT INTO `staff_period` VALUES (595, 3, 104);
INSERT INTO `staff_period` VALUES (596, 4, 104);
INSERT INTO `staff_period` VALUES (597, 5, 104);
INSERT INTO `staff_period` VALUES (598, 100, 104);
INSERT INTO `staff_period` VALUES (599, 101, 104);
INSERT INTO `staff_period` VALUES (600, 102, 104);
INSERT INTO `staff_period` VALUES (601, 104, 104);

-- ----------------------------
-- Table structure for staffs
-- ----------------------------
DROP TABLE IF EXISTS `staffs`;
CREATE TABLE `staffs`  (
  `staffId` int(11) NOT NULL AUTO_INCREMENT,
  `coStaffId` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `staffName` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `position` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `teamId` int(11) NULL DEFAULT NULL,
  `role` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`staffId`) USING BTREE,
  UNIQUE INDEX `coStaffId`(`coStaffId`) USING BTREE,
  INDEX `roleId`(`role`) USING BTREE,
  INDEX `teamId`(`teamId`) USING BTREE,
  CONSTRAINT `teamId` FOREIGN KEY (`teamId`) REFERENCES `teams` (`teamId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of staffs
-- ----------------------------
INSERT INTO `staffs` VALUES (1, 'NV10003', 'Nguyễn Thị Ngọc Thảo', 'HRM', 7, 'LEAD', 'thao.nguyen.11@nhnent.com', '65ded5353c5ee48d0b7d48c591b8f430');
INSERT INTO `staffs` VALUES (2, 'NV10015', 'Trần Tuấn Thành', 'Team Lead', 1, 'LEAD', 'tuanthanh.tran11@nhnent.com', '65ded5353c5ee48d0b7d48c591b8f430');
INSERT INTO `staffs` VALUES (3, 'NV10018', 'Nguyễn Hoàng Diệu', 'Java Developer', 1, 'STAFF', 'hoangdieu.nguyen11@nhnent.com', '65ded5353c5ee48d0b7d48c591b8f430');
INSERT INTO `staffs` VALUES (4, 'NV10021', 'Trần Minh Đức', 'Front-end Developer', 1, 'STAFF', 'minhduc.tran@nhnent.com', '65ded5353c5ee48d0b7d48c591b8f430');
INSERT INTO `staffs` VALUES (5, 'NV10057', 'Võ Tấn Phúc', 'Java Developer', 1, 'STAFF', 'tanphuc.vo@nhnent.com', '65ded5353c5ee48d0b7d48c591b8f430');
INSERT INTO `staffs` VALUES (100, 'Admin', 'Admin', 'Admin', 7, 'ADMIN', 'Admin', '65ded5353c5ee48d0b7d48c591b8f430');
INSERT INTO `staffs` VALUES (101, 'NV10073', 'Dương Thị Thuơng', 'Server Developer', 2, 'STAFF', 'thithuong.duong.11@nhnent.com', '65ded5353c5ee48d0b7d48c591b8f430');
INSERT INTO `staffs` VALUES (102, 'NV10074', 'Kang Dong Won', 'Team Lead', 2, 'LEAD', 'dongwon.kang.11@nhnent.com', '65ded5353c5ee48d0b7d48c591b8f430');
INSERT INTO `staffs` VALUES (103, 'CEO', 'Trùm cuối', 'Trùm cuối', 1, 'ADMIN', NULL, '65ded5353c5ee48d0b7d48c591b8f430');
INSERT INTO `staffs` VALUES (104, 'NV10092', 'Trần Nghĩa Yên', 'Java Developer', 1, 'STAFF', 'nghiayen.tran@nhnent.com', '65ded5353c5ee48d0b7d48c591b8f430');

-- ----------------------------
-- Table structure for teams
-- ----------------------------
DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams`  (
  `teamId` int(11) NOT NULL AUTO_INCREMENT,
  `teamName` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`teamId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teams
-- ----------------------------
INSERT INTO `teams` VALUES (1, 'C-Team');
INSERT INTO `teams` VALUES (2, 'N-Team');
INSERT INTO `teams` VALUES (3, 'Android Development');
INSERT INTO `teams` VALUES (4, 'Design');
INSERT INTO `teams` VALUES (5, 'Game Development');
INSERT INTO `teams` VALUES (6, 'Game Management');
INSERT INTO `teams` VALUES (7, 'HR & GA');
INSERT INTO `teams` VALUES (8, 'iOS Development');
INSERT INTO `teams` VALUES (9, 'QA Team');
INSERT INTO `teams` VALUES (10, 'Service Planning');

SET FOREIGN_KEY_CHECKS = 1;
